const { readdirSync, statSync, writeFileSync } = require('fs');
const { resolve } = require('path');

const blackList = ['.vuepress'];
const root = resolve(__dirname, '../docs/');

function getSidebarMenus(
  subPath = './',
  lastName = '',
  modules = {},
  level = 0,
  link = subPath.substring(1),
  key = link
    .split('/')
    .map((f) => encodeURIComponent(f))
    .join('/'),
) {
  if (key !== '/' && !modules[key]) modules[key] = level < 1 ? {} : [];
  const dirs = [];
  const files = [];

  readdirSync(resolve(root, subPath)).forEach((name) => {
    if (blackList.includes(name)) return;

    const dir = resolve(root, subPath, name);
    const stat = statSync(dir);
    if (stat.isDirectory()) {
      dirs.push(name);
    } else if (!name.endsWith('.md') || name !== 'index.md') {
      files.push(name);
    }
  });

  dirs.forEach((dir) => {
    getSidebarMenus(subPath + dir + '/', dir, level < 1 ? modules : modules[key], level + 1);
  });

  const items = files.map((name) => {
    return {
      text: name.replace(/\.[^\.]+$/, ''),
      link: link + name,
    };
  });
  if (items.length < 1) return modules;

  modules.push({
    text: lastName,
    children: items,
  });

  return modules;
}

const menus = getSidebarMenus();
const dist = resolve(root, '../docs/.vuepress/data/sidebar.json');
writeFileSync(dist, JSON.stringify(menus, null, 2));
