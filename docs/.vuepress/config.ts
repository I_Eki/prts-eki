import { getKatas } from './data/katas';
import vue from '@vitejs/plugin-vue';
import { transformAssetUrls } from '@quasar/vite-plugin';
import sidebar from './data/sidebar.json';

const katas = getKatas();

export default {
  katas,
  title: '亦云阁',
  description: '欢迎来到亦云阁，不过是拾人牙慧，亦或偶得之乐。',
  themeConfig: {
    sidebar,
    navbar: [
      {
        text: '首页',
        link: '/',
      },
      {
        text: '代码',
        children: [
          {
            text: 'Web',
            link: '/编程/Web/',
          },
          {
            text: '前端',
            link: '/编程/前端/',
          },
          {
            text: '后端',
            link: '/编程/后端/',
          },
          {
            text: 'Chrome扩展',
            link: '/编程/Chrome扩展/',
          },
        ],
      },
      {
        text: '读书笔记',
        link: '/读书笔记/',
      },
      {
        text: '项目文档',
        children: [
          {
            text: '亦见',
            link: '/项目文档/亦见/',
          },
        ],
      },
      {
        text: 'Codewars',
        link: '/Katas/',
      },
    ],
    lastUpdated: 'Last Updated',
  },
  extendsMarkdown: (md) => {
    md.use(require('markdown-it-mathjax3'));
  },
  bundlerConfig: {
    vuePluginOptions: [
      vue({
        template: {
          transformAssetUrls,
          compilerOptions: {
            isCustomElement: (tag) => tag === 'mjx-container',
          },
        },
      }),
    ],
  },
  plugins: [require('./plugins/code-click-copy')],
};
