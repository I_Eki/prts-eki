const { path } = require('@vuepress/utils');

module.exports = (options, context) => ({
  define() {
    const { copySelector, copyMessage, failedMessage, duration, showInMobile } =
      options;

    return {
      __GLOBAL_COPY_SELECTOR: copySelector || [
        'div[class*="language-"] pre',
        'div[class*="aside-code"] aside',
      ],
      __GLOBAL_COPY_MESSAGE: copyMessage || 'Copied!',
      __GLOBAL_FAILED_MESSAGE: failedMessage || 'Failed!',
      __GLOBAL_DURATION: duration || 3000,
      __GLOBAL_SHOW_IN_MOBILE: showInMobile || false,
    };
  },

  clientAppSetupFiles: path.resolve(__dirname, './bin/clientAppSetupFiles.js'),
});
