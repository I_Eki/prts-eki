import { defineClientAppEnhance } from '@vuepress/client';
import { Quasar } from 'quasar';
import Kata from './components/Kata.vue';

// Import icon libraries
import '@quasar/extras/material-icons/material-icons.css';

// Import Quasar css
import 'quasar/src/css/index.sass';

// Quasar默认标题字体太大，需要全局覆盖
import './styles/quasar.scss';

export default defineClientAppEnhance(({ app, router, siteData }) => {
  app.use(Quasar, {}, { req: { headers: {} } });
  app.component('Kata', Kata);
  // app.config.compilerOptions.isCustomElement = tag => tag === 'mjx-container';
});
