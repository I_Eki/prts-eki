
### 获取基于 vue-router 定义的路由列表

`typescript` 拥有众多API方法可以自定义分析文件语法，对其进行修改操作，再输出指定内容。但几乎没有文档说明，只有官方的 `Github` 项目上有一篇[Using the Compiler API](https://github.com/Microsoft/TypeScript/wiki/Using-the-Compiler-API)示例代码可以参考。

如果需要使用，一方面可以根据类型声明推测方法的用途和传入参数，另一方面可以使用[AST Explorer](https://astexplorer.net/)(注意选择需要分析的语言类型)和[Typescript AST Viewer](https://ts-ast-viewer.com/)网站辅助分析。

主要原理如下：

1. 通过 `typescript` 的API方法，从文件内容读取生成对应的抽象语法树(`AST`)；
2. 通过分析导出对象及其属性名等，找到路由变量；
3. 由路由节点分析，获取需要的属性名和属性值，以及从对应的注释信息中获取可自定义的页面名称信息；

**注意点：**

1. `findVariableInSourceFile` 方法只遍历了根节点的子节点列表，意味着 `routes` 路由对象的变量定义必须是**此文件模块所在作用域的全局变量**。

```typescript
import * as ts from 'typescript';

/**
 * 从 SourceFile 中查找指定名称的变量节点
 * @param src SourceFile类型节点
 * @param name 需要查找的变量名称
 * @returns 查找到的变量节点，未找到返回undefined
 */
function findVariableInSourceFile(
  src: ts.SourceFile,
  name: string,
): ts.VariableDeclaration | undefined {
  let res: ts.VariableDeclaration;

  // 默认 src 是文件语法树的根节点
  src.statements.some((item) => {
    if (item.kind !== ts.SyntaxKind.VariableStatement) return;
    const variable = (<ts.VariableStatement>(
      item
    )).declarationList.declarations.find((vi) => {
      const nameText = (<ts.Identifier>vi.name).escapedText.toString();
      return nameText === name;
    });
    if (!variable) return;
    res = variable;
    return true;
  });

  return res;
}

/**
 * 通过分析 typescript 生成的 ast 来获取基于 vue-router 定义的路由列表
 * @param path 文件路径
 * @returns 查找到的路由列表
 */
function readRoutesFromFile(path: string): any[] {
  const routes = [];

  try {
    const src = ts.createSourceFile(
      path,
      readFileSync(path).toString(),
      ts.ScriptTarget.ESNext,
    );

    // 读取最后一个导出项
    const exp = (<ts.ExportAssignment>(
      src.statements[src.statements.length - 1]
    )).expression;

    // 将最后一个导出项视为路由对象，读取变量名
    const routerVarName = (<ts.Identifier>exp).escapedText.toString();

    // 查找对应名称的 router 变量的 ast 对象
    const router = this.findVariableInSourceFile(src, routerVarName);
    if (!router) return routes;

    // vue2中使用new关键字创建，vue3中使用函数方法创建
    const routerExp = (<ts.NewExpression | ts.CallExpression>(
      router.initializer
    )).arguments[0];

    // 从 router 变量的 ast 对象中获取 routes 节点
    const propsNode = (<ts.ObjectLiteralExpression>routerExp).properties;
    const routesNode = propsNode.find((prop) => {
      return (<ts.Identifier>prop.name).escapedText === 'routes';
    }) as ts.PropertyAssignment;
    if (!routesNode) return routes;

    let routesExp: ts.Expression;
    if (
      routesNode.initializer.kind === ts.SyntaxKind.ArrayLiteralExpression
    ) {
      // routes是字面量定义，例如{ routes: [ { name: xxx, ... } ] }
      routesExp = routesNode.initializer;
    } else {
      // routes是变量定义，例如{ routes } 或 { routes: myRoutes }
      // 此时先读取对应变量名，再从根节点读取对应变量的 ast 节点
      const routesVarName = (<ts.Identifier>(
        routesNode.initializer
      )).escapedText.toString();
      const routesVar = this.findVariableInSourceFile(src, routesVarName);
      routesExp = routesVar?.initializer;
    }
    if (!routesExp) return routes;

    // 从获取到的 routes 定义节点中读取信息
    (<ts.ArrayLiteralExpression>routesExp).elements.forEach(
      (el: ts.ObjectLiteralExpression) => {
        const route = {};
        el.properties.forEach((prop: ts.PropertyAssignment) => {
          const name = (<ts.Identifier>prop.name).escapedText.toString();
          if (!RouteDtoKeys.includes(name)) return;
          route[name] = (<ts.StringLiteral>prop.initializer).text;
        });

        // 读取注释信息，辅助判断路由所代表的页面名称
        const comment = ts
          .getLeadingCommentRanges(src.text, el.pos)
          ?.slice(-3)
          ?.map((range) => {
            // 替换注释可能存在的标识符
            // 注释无法从节点上读取，只能根据节点的起始位置信息从源内容上截取
            return src.text
              .substring(range.pos, range.end)
              ?.split(/\r\n|\r|\n/)
              .map((line) => {
                return line.replace(
                  /^\s*(\/{2}|\/\*{2}|\*|\*\/)\s*|\*\/\s*$/g,
                  '',
                );
              })
              .join('');
          })
          ?.join('')
          ?.slice(0, 20);
        if (comment) route.title = comment;
        routes.push(route);
      },
    );
    return routes;
  } catch (error) {
    return [];
  }
}
```

#### 支持递归识别路径引入路由信息的抽象类代码

```typescript
// 可直接用于nest项目中

import { HttpService } from '@nestjs/axios';
import { readFileSync, rm } from 'fs';
import { firstValueFrom } from 'rxjs';
import { saveToTempFile } from 'src/utils/utils';
import { RouteDto, RouteDtoKeys, FullRouteDto } from './dto/route.dto';
import cheerio from 'cheerio';
import { ProjectDto } from './dto/proj.dto';
import {
  ArrayLiteralExpression,
  CallExpression,
  createSourceFile,
  ExportAssignment,
  ExportDeclaration,
  Expression,
  getLeadingCommentRanges,
  Identifier,
  ImportDeclaration,
  NamedExports,
  NamedImports,
  NewExpression,
  NodeArray,
  ObjectLiteralExpression,
  PropertyAssignment,
  ScriptTarget,
  SourceFile,
  SpreadElement,
  Statement,
  StringLiteral,
  SyntaxKind,
  VariableDeclaration,
  VariableStatement,
} from 'typescript';
import { Logger } from '@nestjs/common';
import { join } from 'path';

/**
 * 获取远程文件并返回其保存的本地路径
 * @param url 需求请求的文件url路径
 * @param cookie Cookie数据
 * @returns 保存的临时文件路径
 */
export async function requestFile(
  url: string,
  http: HttpService,
  cookie?: string,
  isSave = true,
): Promise<string | undefined> {
  const encodedUrl = encodeURI(url);
  const req = http.get(encodedUrl, {
    headers: {
      Cookie: cookie,
    },
  });

  try {
    const ext = url.match(/(?<=[\/\\]\w+\.)\w+(?=[^\/\\]*$)/)?.[0] || '';
    const res = await firstValueFrom(req);
    if (res.status !== 200) return;
    return isSave ? saveToTempFile(res.data, ext) : res.data;
  } catch (error) {
    Logger.warn(error);
  }
}

/**
 * 从html文档中读取项目列表数据
 * @param html html字符串内容
 */
export function getProjectsFromHTML(html: string): Partial<ProjectDto>[] {
  let $;
  try {
    $ = cheerio.load(html);
  } catch (error) {
    return [];
  }

  const fullnames = $('.project-row .project-full-name').text();
  return fullnames
    .replace(/\s+[\/\\]\s+/g, '/')
    .trim()
    .split('\n')
    .map((line) => {
      const [system = '', namespace = '', project = ''] = line.split('/');
      return {
        system,
        namespace,
        project,
      };
    });
}

/**
 * 从html文档中读取项目代码分支列表
 * @param html html字符串内容
 */
export function getBranchesFromHTML(html: string): string[] {
  let $;
  try {
    $ = cheerio.load(html);
  } catch (error) {
    return [];
  }

  const branchItems = $('.branch-item');
  return branchItems
    .map(function () {
      return $(this).data('name');
    })
    .get();
}

export class SourceFileHelper {
  /**
   * 从 SourceFile 中查找指定名称的变量节点
   * @param src SourceFile类型节点
   * @param name 需要查找的变量名称
   * @returns 查找到的变量节点，未找到返回undefined
   */
  static findVariableInSourceFile(
    src: SourceFile,
    name: string,
  ): VariableDeclaration | VariableDeclaration | undefined {
    let res: VariableDeclaration | undefined;

    src.statements.some((item) => {
      if (item.kind === SyntaxKind.VariableStatement) {
        const variable = (<VariableStatement>(
          item
        )).declarationList.declarations.find((vi) => {
          const nameText = (<Identifier>vi.name)?.escapedText?.toString();
          return nameText === name;
        });
        if (!variable) return;
        res = variable;
        return true;
      }
    });

    return res;
  }

  /**
   * 从 SourceFile 中查找指定名称的导入路径
   * @param src SourceFile类型节点
   * @param name 需要查找的变量名称
   * @returns 查找到的导入变量的路径，未找到返回空字符串
   */
  static findImportPathInSourceFile(src: SourceFile, name: string): string[] {
    let res = [];

    src.statements.some((item) => {
      res = [];
      if (item.kind !== SyntaxKind.ImportDeclaration) return;
      const clause = (<ImportDeclaration>item).importClause;
      let isThisItem = false;
      if (clause.name) {
        isThisItem = clause.name.escapedText.toString() === name;
        res.push('default');
      } else {
        isThisItem = (<NamedImports>clause.namedBindings).elements.some(
          (el) => {
            const text = el.name.escapedText.toString();
            if (text !== name) return false;
            res.push(text);
            return true;
          },
        );
      }
      if (!isThisItem) return;
      res.push((<StringLiteral>(<ImportDeclaration>item).moduleSpecifier).text);
      return true;
    });

    return res;
  }

  // readDefaultExport() {}
}

export class RoutesSniffer {
  private tempPath = '';
  private src: SourceFile;

  constructor(
    private readonly http: HttpService,
    private readonly projectRoot: string,
    private readonly path: string,
    private readonly cookie: string,
    private readonly varName = 'default',
  ) {}

  /**
   * 获取远程文件并完成 AST 初始化
   * @returns {Boolean} 是否初始化成功
   */
  async init(tempPath?: string) {
    if (tempPath) {
      this.tempPath = tempPath;
      this.src = createSourceFile(
        this.tempPath,
        readFileSync(this.tempPath).toString(),
        ScriptTarget.ESNext,
      );
      return true;
    }

    const url = [
      this.projectRoot.replace(/\/+$/, ''),
      this.path.replace(/^\/+/, ''),
    ].join('/');
    let result = false;

    try {
      this.tempPath = await requestFile(url, this.http, this.cookie);

      this.src = createSourceFile(
        this.tempPath,
        readFileSync(this.tempPath).toString(),
        ScriptTarget.ESNext,
      );

      result = true;
    } catch (error) {
    } finally {
      this.finished();
      return result;
    }
  }

  /** 探测路由列表 */
  async sniffRoutes(): Promise<Partial<FullRouteDto>[]> {
    const routes: Partial<FullRouteDto>[] = [];
    let exp: Statement | VariableDeclaration;

    if (this.varName === 'default') {
      // 读取默认导出项
      exp = this.src.statements.find((item) => {
        return item.kind === SyntaxKind.ExportAssignment;
      });
    } else {
      // const routes = [...]; export { routes }
      exp = this.src.statements.find((item) => {
        return (
          item.kind === SyntaxKind.ExportDeclaration &&
          (<NamedExports>(
            (<ExportDeclaration>item).exportClause
          ))?.elements[0]?.name?.escapedText.toString() === this.varName
        );
      });
      if (!exp) {
        // export const routes = [...];
        exp = SourceFileHelper.findVariableInSourceFile(
          this.src,
          this.varName,
        ) as unknown as Statement;
      }
    }
    if (!exp) return routes;

    // 如果节点是直接导入的变量类型，直接调用方法获取，不执行查找列表节点的代码
    if (
      (<ExportAssignment>exp).expression?.kind ===
      SyntaxKind.ArrayLiteralExpression
    ) {
      // export default [...]
      const exportDefaultRoutes = await this.readRoutesFromNodeArray(
        this.src,
        (<ExportAssignment>exp).expression as ArrayLiteralExpression,
      );
      return exportDefaultRoutes;
    } else if (
      (exp as unknown as VariableDeclaration).initializer?.kind ===
      SyntaxKind.ArrayLiteralExpression
    ) {
      // export const xxx = []
      const exportVariableRoutes = await this.readRoutesFromNodeArray(
        this.src,
        (exp as unknown as VariableDeclaration)
          .initializer as ArrayLiteralExpression,
      );
      return exportVariableRoutes;
    }

    // 将导出项视为路由对象，读取变量名
    const routerVarName = (<Identifier>(
      (<ExportAssignment>exp).expression
    ))?.escapedText.toString();

    if (!routerVarName) return routes;

    // 查找对应名称的 router 变量的 ast 对象
    const router = SourceFileHelper.findVariableInSourceFile(
      this.src,
      routerVarName,
    );
    if (!router) return routes;

    // vue2中使用new关键字创建，vue3中使用函数方法创建
    const routerExp = (<NewExpression | CallExpression>router.initializer)
      .arguments[0];

    // 从 router 变量的 ast 对象中获取 routes 节点
    const propsNode = (<ObjectLiteralExpression>routerExp).properties;
    const routesNode = propsNode.find((node) => {
      return (<Identifier>node.name).escapedText === 'routes';
    }) as PropertyAssignment;
    if (!routesNode) return routes;

    const realRoutes = await this.readRoutesFromNodeArray(this.src, routesNode);
    return realRoutes;
  }

  /**
   * 从声明节点中读取路由列表数据
   * @param src 代码 ast 根节点
   * @param routesDl 路由列表声明节点
   * @returns
   */
  async readRoutesFromNodeArray(
    src: SourceFile,
    routesDl: VariableDeclaration | PropertyAssignment | ArrayLiteralExpression,
  ) {
    const routes: RouteDto[] = [];
    let elements: NodeArray<Expression>;

    if (routesDl.kind === SyntaxKind.ArrayLiteralExpression) {
      elements = routesDl.elements;
    } else {
      let routesExp: Expression;

      // 获取路由项节点列表
      if (routesDl.initializer?.kind === SyntaxKind.ArrayLiteralExpression) {
        // routes是字面量定义，例如{ routes: [ { name: xxx, ... } ] }
        routesExp = routesDl.initializer;
      } else if (
        !routesDl.initializer ||
        routesDl.initializer?.kind === SyntaxKind.Identifier
      ) {
        // routes是变量定义，例如{ routes } 或 { routes: myRoutes }
        // 此时先读取对应变量名，再从根节点读取对应变量的 ast 节点
        const routesVarName = (<Identifier>(
          (routesDl.initializer || routesDl.name)
        )).escapedText.toString();
        const routesVar = SourceFileHelper.findVariableInSourceFile(
          this.src,
          routesVarName,
        );
        routesExp = routesVar?.initializer;
      }

      if (!routesExp) return routes;

      elements = (<ArrayLiteralExpression>routesExp).elements;
    }

    // 从获取到的 routes 项列表子节点中读取信息
    for (const el of elements) {
      if (el.kind === SyntaxKind.ObjectLiteralExpression) {
        // 为真实定义代码，读取数据
        const route = this.readRouteFromOle(src, el);
        routes.push(route);
      } else if (el.kind === SyntaxKind.SpreadElement) {
        const varExp = (<SpreadElement>el).expression;
        const varName = (<Identifier>varExp).escapedText.toString();
        const varNode = SourceFileHelper.findVariableInSourceFile(src, varName);
        if (varNode) {
          // 变量引用，例如routes: [...childRoutes]，且childRoutes在本文件作用域中有定义
          const spreadRoutes = await this.readRoutesFromNodeArray(src, varNode);
          routes.push(...spreadRoutes);
        } else {
          // 变量引用，例如routes: [...childRoutes]，但childRoutes为从其它模块文件导入
          const [importName, importPath] =
            SourceFileHelper.findImportPathInSourceFile(src, varName);
          const importedRoutes = await this.readRoutesFromImport(
            importName,
            importPath,
          );
          routes.push(...importedRoutes);
        }
      }
    }

    return routes;
  }

  /** 从导入名称和路径获取对应的路由数据 */
  async readRoutesFromImport(
    varName,
    relativePath,
  ): Promise<Partial<FullRouteDto>[]> {
    if (!varName || !relativePath) return [];

    const basePath = join(this.path.replace(/\/[^\/]+$/, ''), relativePath);
    const paths = [basePath.replace(/\\/g, '/')];

    // 检查缩写情况，考虑后缀缩写和index名称省略
    if (!/\.[^\.]+$/.test(paths[0])) {
      const exts = ['js', 'ts', 'jsx', 'tsx'];
      exts.forEach((ext) => {
        paths.push(`${paths[0]}.${ext}`);
        paths.push(`${paths[0]}/index.${ext}`);
      });
      paths.shift();
    }

    for (const path of paths) {
      // 对每一个可能的路径文件做路由探测
      const sniffer = new RoutesSniffer(
        this.http,
        this.projectRoot,
        path,
        this.cookie,
        varName,
      );
      const initRes = await sniffer.init();
      if (!initRes) continue;

      const routes = await sniffer.sniffRoutes();
      // 探测到有效路由数据后跳出
      if (routes.length > 0) return routes;
    }

    return [];
  }

  /**
   * 从单个路由项节点中获取路由数据
   * @param src 源码 ast 根节点
   * @param node 单个路由项节点
   */
  readRouteFromOle(src: SourceFile, node: Expression) {
    const route: RouteDto = {};
    (<ObjectLiteralExpression>node).properties?.forEach(
      (prop: PropertyAssignment, index) => {
        const name = (<Identifier>prop.name).escapedText.toString();
        if (!RouteDtoKeys.includes(name)) return;
        route[name] = (<StringLiteral>prop.initializer).text;
        if (index === 0) {
          const nameComment = this.getComment(src, prop.pos);
          if (nameComment) {
            route.title = nameComment;
          }
        }
      },
    );

    if (!route.title) {
      const comment = this.getComment(src, node.pos);
      if (comment) route.title = comment;
    }

    return route;
  }

  /** 读取注释信息，辅助判断路由所代表的页面名称 */
  getComment(src: SourceFile, pos: number) {
    const ranges = getLeadingCommentRanges(src.text, pos);
    const comment = ranges
      ?.slice(-1)
      .map((range) => {
        // 替换注释可能存在的标识符
        return src.text
          .substring(range.pos, range.end)
          ?.split(/\r\n|\r|\n/)
          .map((line) => {
            return line.replace(/^\s*(\/{2}|\/\*{2}|\*|\*\/)\s*|\*\/\s*$/g, '');
          })
          .join('');
      })
      .join('');
    return comment;
  }

  /**
   * 操作结束后的回调
   */
  finished() {
    if (!this.tempPath) return;
    rm(this.tempPath, { force: true }, () => void 0);
  }
}
```
