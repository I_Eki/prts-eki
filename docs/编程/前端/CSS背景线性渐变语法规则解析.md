### CSS背景线性渐变语法规则解析

#### 一、线形渐变的构成

*摘录自[MDN](https://developer.mozilla.org/zh-CN/docs/Web/CSS/gradient/linear-gradient)*

1. 线性渐变由一个轴 (梯度线) 定义，其上的每个点具有两种或多种的颜色，且轴上的每个点都具有独立的颜色。为了构建出平滑的渐变，linear-gradient() 函数构建一系列垂直于渐变线的着色线，每一条着色线的颜色则取决于与之垂直相交的渐变线上的色点；

![](https://developer.mozilla.org/en-US/docs/Web/CSS/gradient/linear-gradient/linear-gradient.png)

2. 渐变线由包含渐变图形的容器的中心点和一个角度来定义的。渐变线上的颜色值是由不同的点来定义，包括起始点，终点，以及两者之间的可选的中间点（中间点可以有多个）；

3. 起始点是渐变线上代表起始颜色值的点。起始点由渐变线和过容器顶点的垂直线之间的交叉点来定义。（垂直线跟渐变线在同一象限内）；

4. 同样的，终点是渐变线上代表最终颜色值的点。终点也是由渐变线和从最近的顶点发出的垂直线之间的交叉点定义的，然而从起始点的对称点来定义终点是更容易理解的一种方式，因为终点是起点关于容器的中心点的反射点。

#### 二、语法

*参考[语法](https://developer.mozilla.org/zh-CN/docs/Web/CSS/gradient/linear-gradient#%E8%AF%AD%E6%B3%95)*

*个人注解：*

1. 如果渐变点不带有角度信息，则默认按照 `to bottom` 轴即 `180deg` 或 `0.5turn` 渲染。

<div class="box" style="display: inline-block; width: 100px; height: 100px; background-image: linear-gradient(skyblue, orangered);"></div>

<div class="box" style="margin-left: 10px; display: inline-block; width: 100px; height: 100px; background-image: linear-gradient(0, skyblue, orangered);"></div>

```html
<!-- Demo -->

<style>
  .box {
    width: 100px;
    height: 100px;
    /* 无角度信息 */
    background-image: linear-gradient(skyblue, orangered);
    /* 带有角度信息 */
    /* background-image: linear-gradient(0, skyblue, orangered); */
  }
</style>

<div class="box"></div>
```

2. 渐变渲染只能从颜色点较前的位置向较后的位置渲染，如果在某个颜色点的位置数据比前一个颜色点的位置更靠前，则前一个颜色点将渲染到100%的位置，再取最后一个颜色点的颜色作为基准颜色，直接填充之前未定义到的部分（也可以理解为将剩下的颜色点位置都当作是100%并且无渐变，由于互相覆盖，所以最后只能看到最后一个颜色）。

<div class="box" style="display: inline-block; width: 100px; height: 100px; background-image: linear-gradient(skyblue 50%, red 75%, orangered 0, brown 40%, yellow 10%, green 20%);"></div>

```html
<!-- Demo -->

<style>
  .box {
    display: inline-block;
    width: 100px;
    height: 100px;
    background-image: linear-gradient(
      skyblue 50%,
      red 75%,
      orangered 0,
      brown 40%,
      yellow 10%,
      green 20%
    );
  }
</style>

<div class="box"></div>
```

从上面的样式中可以看到，颜色点共有三个阶段：50% ~ 75%、0 ~ 40%、10% ~ 20%，实际渲染的阶段为0% ~ 50%填充skyblue、50% ~ 75%的渐变，然后取了最后一个颜色点的绿色，直接填充了75% ~ 100%部分。

#### 三、黑白格背景解析

原样式代码：

<div class="box" style="display: inline-block; width: 240px; height: 160px; background: #eee; background-image: linear-gradient(45deg, #bbb 25%, transparent 0), linear-gradient(45deg, transparent 75%, #bbb 0), linear-gradient(45deg, #bbb 25%, transparent 0), linear-gradient(45deg, transparent 75%, #bbb 0); background-position: 0 0, -20px 20px, 20px -20px, 0 0; background-size: 40px 40px;"></div>

```css
.box {
  background: #eee;
  background-image:
    linear-gradient(45deg, #bbb 25%, transparent 0),
    linear-gradient(45deg, transparent 75%, #bbb 0),
    linear-gradient(45deg, #bbb 25%, transparent 0),
    linear-gradient(45deg, transparent 75%, #bbb 0);
  background-position: 0 0, -20px 20px, 20px -20px, 0 0;
  background-size: 40px 40px;
}
```

1. `background: #eee;` 渲染背景底色，也就是说使用渐变实现的只是深色格子的部分；
2. 先看 `background-size: 40px 40px;`，规定了背景大小，而没有限制背景不重复，所以我们可以只分析 `40x40` 大小的格子，其余部分都是这个大小背景的重复；

<div class="box" style="display: inline-block; width: 40px; height: 40px; background: #eee; background-image: linear-gradient(45deg, #bbb 25%, transparent 0), linear-gradient(45deg, transparent 75%, #bbb 0), linear-gradient(45deg, #bbb 25%, transparent 0), linear-gradient(45deg, transparent 75%, #bbb 0); background-position: 0 0, -20px 20px, 20px -20px, 0 0; background-size: 40px 40px;"></div>

3. 可以发现 `background-image` 虽然由四个渐变组成，但是后两个是前两个的重复，那么如果先不设置 `background-position`，并且拿掉重复的后两个渐变，初始样式如下：

<div class="box" style="display: inline-block; width: 40px; height: 40px; background: #eee; background-image: linear-gradient(45deg, #bbb 25%, transparent 0), linear-gradient(45deg, transparent 75%, #bbb 0); background-size: 40px 40px;"></div>

```css
.box {
  width: 40px;
  height: 40px;
  background: #eee;
  background-image:
    linear-gradient(45deg, #bbb 25%, transparent 0),
    linear-gradient(45deg, transparent 75%, #bbb 0);
  background-size: 40px 40px;
}
```

4. 到这一步就可以明白实现的核心原理了，大致如下：

  + 限定单个背景格子大小为一个正方形，并且让其自动重复渲染，填满整个背景；
  + 单个格子中使用两个 `45deg` 渐变轴，填充 `0 ~ 25%` 部分绘制左下角深色三角形，填充 `75% ~ 100%` 部分绘制右上角深色三角形；
  + 重复上面的背景一次，并且调整重复的背景位置（注意 `background-position` 的多个位置是和 `background-image` 的渐变背景一一对应的），用四个三角形拼合成两个深色小正方形；

<div class="box" style="display: inline-block; width: 240px; height: 160px; background: #eee; background-image: linear-gradient(45deg, #bbb 25%, transparent 0), linear-gradient(45deg, transparent 75%, #bbb 0); background-size: 40px 40px;"></div>

```css
.box {
  width: 240px;
  height: 160px;
  background: #eee;
  background-image:
    linear-gradient(45deg, #bbb 25%, transparent 0),
    linear-gradient(45deg, transparent 75%, #bbb 0);
  background-size: 40px 40px;
}
```

5. 拼合可以有两种方式：一种是平行于渐变轴，即 `45deg` 方向，向右上或左下移动 `20x20` 的距离，即开头样式代码；第二种是垂直于渐变轴，向左上或右下移动 `20x20` 的距离，样式代码如下：

<div class="box" style="display: inline-block; width: 240px; height: 160px; background: #eee; background-image: linear-gradient(45deg, #bbb 25%, transparent 0), linear-gradient(45deg, transparent 75%, #bbb 0), linear-gradient(45deg, #bbb 25%, transparent 0), linear-gradient(45deg, transparent 75%, #bbb 0); background-position: 0 0, 0 0, 20px 20px, 20px 20px; background-size: 40px 40px;"></div>

```css
.box {
  background: #eee;
  background-image:
    linear-gradient(45deg, #bbb 25%, transparent 0),
    linear-gradient(45deg, transparent 75%, #bbb 0),
    linear-gradient(45deg, #bbb 25%, transparent 0),
    linear-gradient(45deg, transparent 75%, #bbb 0);
  background-position: 0 0, 0 0, 20px 20px, 20px 20px;
  background-size: 40px 40px;
}
```
