
### RAF动画循环，支持锁帧

#### 说明

`v-fps` 指令需要传入一个作为 `requestAnimationFrame` 方法的参数的回调函数。

+ 可使用 `v-fps.lock="xxx"` 声明动画需要锁帧，避免高刷屏上动画过快（不考虑老机型丢帧问题）。

+ 快速实现组件加载时执行动画，并且在 `DOM` 卸载后清除动画函数。

+ 可在回调函数中使用 `return false` 来停止动画，停止后需要有组件 `DOM` 重载操作，才能再次唤起动画。

#### 源码

```javascript
import Vue from 'vue'

Vue.directive('fps', {
  inserted(el, binding, vnode) {
    if (!vnode.$fps) {
      vnode.$fps = {
        raf: null,
        callback: binding.value,
        lockFrame: binding.modifiers.lock,
        lastTime: null
      }
    }

    const { raf, lockFrame, lastTime, callback } = vnode.$fps

    window.cancelAnimationFrame(raf)

    if (!callback || typeof callback !== 'function') return

    const rafCallback = elapsed => {
      if (lockFrame && lastTime && elapsed - lastTime < 16.6666666) {
        vnode.$fps.raf = requestAnimationFrame(rafCallback)
        return
      }

      vnode.$fps.lastTime = elapsed

      const res = callback(elapsed)
      if (res !== false) {
        vnode.$fps.raf = requestAnimationFrame(rafCallback)
      }
    }

    vnode.$fps.raf = requestAnimationFrame(rafCallback)
  },
  unbind(el, binding, vnode) {
    if (vnode.$fps?.raf) {
      window.cancelAnimationFrame(vnode.$fps.raf)
      vnode.$fps.raf = null
    }
  }
})
```

#### Demo

实现一个从左向右滚动轮播信息的组件

```vue
<template>
  <div class="marquee-bar" :style="cssVar" v-fps.lock="loop">
    <div class="text current" ref="current">{{ currentText }}</div>
    <div class="text next">{{ nextText }}</div>
  </div>
</template>

<script>
export default {
  name: 'MarqueeBar',
  props: {
    msgs: {
      type: Array,
      default: () => []
    }
  },
  data() {
    return {
      index: 0,
      translateX: 0,
      speed: 1,
      gap: 200,
      raf: null
    }
  },
  computed: {
    currentText() {
      return this.msgs[this.index] || '空'
    },
    nextText() {
      if (this.msgs.length < 1) return ''

      const nextIndex = (this.index + 1) % this.msgs.length

      return this.msgs[nextIndex]
    },
    cssVar() {
      return {
        '--transX': this.translateX + 'px',
        '--gap': this.gap + 'px'
      }
    }
  },
  methods: {
    next() {
      this.translateX = 0
      this.index = (this.index + 1) % this.msgs.length
    },
    loop() {
      if (this.msgs.length < 1) return false
      this.translateX -= this.speed
      const currentTextWidth = this.$refs.current?.clientWidth
      if (Math.abs(this.translateX) > currentTextWidth + this.gap) {
        this.next()
      }
    }
  }
}
</script>

<style lang="less" scoped>
.marquee-bar {
  height: 30px;
  line-height: 30px;
  font-size: 16px;
  color: #fff;
  overflow: hidden;
  display: flex;
  align-items: center;

  .text {
    transform: translateX(var(--transX));
    white-space: nowrap;

    + .text {
      margin-left: var(--gap);
    }
  }
}
</style>
```
