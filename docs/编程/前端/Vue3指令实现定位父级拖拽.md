
### Vue3指令实现定位父级拖拽

#### 说明

最近有个项目，需要实现按住某个悬浮的列表窗，可以在父级容器内随意拖动。听上去非常简单，但要拥有良好体验，也是走了不少弯路，于是将中间遇到的问题记录一下，避免以后再在同样的问题上浪费时间。

1. 原生 `drag` 相关事件的拖动体验不错，但语义化不友好，且快速拖动时会呈现原生的虚影样式；

2. 鼠标事件如果全部绑定在子盒子上，快速拖动时会丢失目标，一开始考虑挂载到全局，后来发现可以用 `Element.setCapture` 方法固定事件目标，但此方法已逐步废弃，改用 `Element.setPointerCapture` 方法和 `pointer` 相关事件；

3. 因为实现的是自动寻找父级绝对定位的元素，会出现闭包；

#### 代码

```javascript
app.directive('abs-drag', {
  mounted(el) {
    let node = el.parentElement;
    // 寻找最近父级绝对定位的元素
    while (node) {
      const style = window.getComputedStyle(node);
      const position = style.position;
      if (position === 'absolute') {
        // 保存transform样式信息
        el.dataset.transform =
          style.transform === 'none'
            ? 'matrix(1, 0, 0, 1, 0, 0)'
            : style.transform;
        el.dataset.x = 0;
        el.dataset.y = 0;
        break;
      }
      node = node.parentNode;
    }
    if (!node) return;

    // 绑定光标事件
    el.addEventListener('pointerenter', (e: PointerEvent) => {
      const node = e.target as HTMLElement;
      node.dataset.drag = '0';
    });
    el.addEventListener('pointerdown', (e: PointerEvent) => {
      const { x, y } = e;
      const node = e.target as HTMLElement & Element;
      node.dataset.drag = '1'; // 标识可以开始拖拽
      node.dataset.x = x + '';
      node.dataset.y = y + '';
      node.setPointerCapture(e.pointerId);
    });
    el.addEventListener('pointerup', (e: PointerEvent) => {
      el.dataset.drag = '0';
      (e.target as Element).releasePointerCapture(e.pointerId);
    });
    el.addEventListener('pointermove', (e: PointerEvent) => {
      const { drag, transform, x: x0, y: y0 } = el.dataset;
      if (drag !== '1') return;
      const { x, y } = e;
      const matrix = transform
        .substring(7, transform.length - 1)
        .split(/,\s*/);
      // 修改坐标偏移量
      matrix[4] = +matrix[4] + x - x0;
      matrix[5] = +matrix[5] + y - y0;
      const newTransform = `matrix(${matrix.join(', ')})`;
      node.style.transform = newTransform;
      el.dataset.transform = newTransform;
      // 更新相对坐标数据
      el.dataset.x = x;
      el.dataset.y = y;
    });
  },
});
```

#### Demo

```vue
<template>
  <div class="box">
    <div class="container">
      <div class="item" v-abs-drag></div>
    </div>
  </div>
<template>

<style scoped>
.box {
  position: relative;
  width: 600px;
  height: 400px;
  background-color: bluesky;
}

.container {
  position: absolute;
  left: 0;
  top: 0;
  width: 100px;
  height: 300px;
  background-color: grey;
}

.item {
  width: 100%;
  height: 40px;
  background-color: orangered;
}
</style>
```
