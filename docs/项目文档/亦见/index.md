# 亦见

## 项目简介

`亦见` 是[亦云](https://yiyun.eki.space/)项目的子模块，用于辅助实现移动端拍照后进行文字识别，然后将结果同步到PC或平板等易于进行文字工作的客户端，同时兼具Markdown、LaTeX等语法渲染支持和文件下载导出，方便将编辑内容转移到其它应用进行二次处理。

## 部署地址

+ Web页面：[https://yiyun.eki.space/yijian](https://yiyun.eki.space/yijian)
+ 小程序：
  - 体验版（需要申请权限）：

  <img src="/assets/images/yijian/qrcode_exp.jpg" alt="体验版二维码" width="100" height="100" />

## 项目地址

+ Web页面：[https://bitbucket.org/I_Eki/yiyun-web/](https://bitbucket.org/I_Eki/yiyun-web/)
+ 小程序：[https://bitbucket.org/I_Eki/yijian-mp/](https://bitbucket.org/I_Eki/yijian-mp/)

## 主要原理

### 流程

+ 小程序流程
  1. 使用原生相机组件拍照，得到图片的base64编码信息；
  2. 使用 `image-cropper` 进行自定义旋转、裁剪；
  3. 遍历调用 `OCR` 文字识别服务供应商的 `API` 接口，得到识别后的文字信息数据；
  4. 将数据分块同步至 `pusher`；

+ Web页面流程
  1. 与 `pusher` 建立长连接；
  2. 接收分块数据，并在所有独立分块接收完成后格式化数据，并推入数据池；
  3. 从数据池中取出文字内容，使用 `ace` 和 `stackedit` 编辑内容；
  4. 监听编辑内容更改，并实时使用 `markdown-it` 渲染；
  5. 使用 `html2canvas` 和 `jsPDF` 实现根据渲染内容自动生成 `PDF` 和 `HTML` 等格式的文件并导出；

### 痛点

1. `pusher` 单次请求支持的数据块长度有限制，所以需要对数据对象进行 `JSON` 序列化后执行分块操作，此处设计仅保证数据完整性，但未对非完整数据的过期二次请求做适配；
2. `PDF` 文件大小默认设置为A4纸大小，但对于过长内容，需要手动实现分页；
3. ~~`codemirror` 在部分情况下粘贴或自动插入内容会导致内部行列计算出错，尚不清楚原因；~~
已使用 `Ace` 和 `Stackedit` 替换；

## 使用说明

此项目模块设计用于轻便照相端和文字处理端协同工作，所以需要一个可以使用微信小程序的手机，以及一个能够访问Web页面和方便进行快速文字录入的设备（PC或平板电脑）。

### 准备

1. **打开小程序：**请扫描[项目地址](#项目地址)中的小程序二维码或是在微信中搜索 `那夙斯Nusuth` 相关关键词，打开小程序；
2. **打开Web页面：**请访问[项目地址](#项目地址)中的页面地址，并确保左上方 `pusher` 连接状态标识如图所示。

  <img src="/assets/images/yijian/pusher_connect_status.png" alt="连接状态" />

### 使用

1. 点击小程序底部切换到 `OCR` Tab页，选择好API提供者和其它配置后点击 `拍照` 或 `从相册中选择` 进行拍照或选择手机本地图片上传。如果未勾选 `不裁剪`，在图片上传前需要执行裁剪操作。
2. 确认后，将自动上传图片进行文字识别，成功返回后右下角纸飞机图标状态变为可用，点击将数据同步至 `pusher`，或勾选自动同步。
3. 等待1-2s（取决于网络速度），此时在已打开且 `pusher` 连接状态为 `已连接` 的本项目Web页面上，左侧数据池列表将会出现同步过来的数据项，程序自动选中新同步的项，即可开始编辑。

### Markdown语法支持

#### 代码块语言支持

目前支持语法高亮的语言包括：

+ 核心
  - C-Like(包括C、C#、C++等)
  - Web-Lang(HTML、JavaScript、CSS)
  - Markdown

+ 增强
  - bash
  - CoffeeScript
  - JSON(JSON5、JSONP)
  - LESS
  - LUA
  - MongoDB
  - nginx
  - Python
  - SCSS
  - SQL
  - TypeScript
  - vbscript
  - visual basic
  - yaml


#### 非编程语言支持

目前支持 `LaTeX` 格式的数学公式语言，用 `$` 符号包裹，如 `$1/2$`，将渲染为 $1/2$。

*注：Stackedit的渲染器支持流程图和Toc目录等，基于Ace实现的工具栏可使用图床上传功能，可配合使用。*
