# CSS选择器世界

## 第 1 章 概述

### 1.1 基本概念

#### 1.1.1 分类

1. **选择器：**标签、类名、ID、属性等；
2. **选择符：**
  + 后代关系：空格（ ）
  + 父子关系：尖括号（>）
  + 相邻兄弟关系：加号（+）
  + 兄弟关系：波浪连接符（~）
  + 列关系：双管道（||）
3. **伪类：**通常与浏览器行为和用户行为相关，具有前缀单冒号（:）；
4. **伪元素：**具有前缀双冒号（::），常见的有::before、::after、::first-letter、::first-line等。

#### 1.1.2 作用域

伪类:scope可实现css的局部作用域，但此特性目前基本被废弃（但在JS中有效，参考[其他伪类选择器](#第12章-其他伪类选择器)章节）。

```html
<section>
  <style scoped>
    p {
      color: blue;
    }

    :scope {
      background-color: red;
    }
  </style>
  <p>在作用域内，背景色应该红色。</p>
</section>
<p>在作用域外，默认背景色。</p>
```

以上代码中section标签内的p标签的背景色理论上应是红色，但无浏览器表现为红色。

**另外**，Shadow DOM中的CSS选择器样式也具有局部作用域。

```html
<p>一个普通的&lt;p&gt;元素，背景色为透明</p>
<div id="hostElement"></div>
```

```javascript
// 创建 Shadow DOM
var shadow = hostElement.attachShadow({
  mode: 'open'
});
// 给 Shadow DOM 添加文字
shadow.innerHTML = '<p>由 Shadow DOM创建的&lt;p&gt;元素，背景色是黑色</p>';
// 添加 CSS，设置p标签背景色为黑色
shadow.innerHTML += '<style>p { background-color: #333; color: #fff; }</style>';
```

#### 1.1.3 命名空间

HTML代码：

```html
<html xmlns="http://www.eki.space">

<body>
  <svg xmlns="http://www.eki.space/svg">
</body>

</html>
```

CSS代码：

```CSS
@namespace url(http://www.eki.space);
@namespace svg url(http://www.eki.space/svg);

/* XHTML中的 <a> 元素 */
a {}

/* SVG中的 <a> 元素 */
svg|a {}

/* 同时匹配XHTML和SVG中的 <a> 元素 */
*|a {}
```

如上所示，可使用命名空间解决CSS样式的冲突问题，且兼容性好，但很少见到有人在项目中使用。因为在HTML中直接内联SVG的应用场景不多，且由更简单的替代方案，不需要掌握复杂的命名空间语法。例如：

```CSS
svg a {
  color: black;
}
```

### 1.2 无效CSS选择器特性和实际应用

当无效的 CSS 选择器和浏览器支持的 CSS 选择器写在一起的时候，会导致整个选择器无效。例如：

```CSS
/* 支持的选择器 */
.x:hover,
.x:active,
/* IE 浏览器不支持的选择器 */
.x:focus-within {
  color: red;
}

/* 以上样式在 IE 浏览器中不会生效 */
```

为了兼容IE，需要分开写：

```CSS
/* IE 支持 */
.x:hover,
.x:active {
  color: red;
}

/* IE 浏览器不支持的选择器 */
.x:focus-within {
  color: red;
}
```

**例外：**浏览器可以识别任何以-webkit-私有前缀开头的伪元素选择器，此特性除 IE 浏览器外均支持，可用于区分：

```CSS
/* IE 浏览器 */
.x {
  background: black;
}

/* 其他浏览器 */
.x,
::-webkit-any {
  background: red;
}

/* 注意：浏览器只是能识别这种书写格式的选择器而正常渲染写在一起的其他选择器的样式，这种随便写的选择器实际上是无法正常使用的 */
```

## 第 2 章 CSS选择器的优先级

### 2.1 规则概览

1. 0级
    - 通配选择器：*
    - 选择符：+、>、~、空格和||
    - 逻辑组合伪类：:not()、:is()、:where()等，这些伪类不影响CSS优先级，但括号内的选择器会影响
2. 1级
    - 标签选择器
3. 2级
   + 类选择器
   + 属性选择器
   + 伪类
4. 3级
   + ID选择器
5. 4级
   + style属性内联
6. 5级
   + !important

!important 是顶级优先级，唯一推荐使用的场景是使JavaScript设置无效。例如：

```CSS
.foo[style*="color: #ccc"] {
  color: #fff !important;
}
```

### 2.2 深入

#### 2.2.1 计算规则

此处基础知识请查询网络资料。

***注意：***无论是文档内部，还是不同文档，相同权重的选择器样式都遵循“**后来居上**”的规则。

**技巧：**重复选择器自身增加优先级

```CSS
/* 如果要从父级组件更改子组件的样式而不污染其他地方 */
.foo {
  color: #333;
}

/* 不推荐的做法，因为标签名或类名可能产生变化 */
.parent .foo {}

div.foo {}

.foo.foo-xxx {}

/* 推荐的做法 */
.foo.foo {}

.foo[class] {}

#foo[id] {}
```

#### 2.2.2 权重越级

```html
<span id="foo" class="f">颜色是？</span>
```

```CSS
#foo {
  color: #000;
  background: #eee;
}

.f {
  color: #fff;
  background: #333;
}
```

在 IE 浏览器中，如果将.f类名重复书写超过256次，即

```CSS
/* 重复256次 */
.f.f.f.......f.f {
  color: #fff;
  background #333;
}
```

则类选择器的样式会覆盖id选择器的样式。因为以前的类名都是用8字节字符串存储，最多容纳255个，超出时则会溢出到ID区域，现在采用了16字节字符串存储，除了IE不会看到上述现象。

## 第 3 章

### 3.1 选择器是否区分大小写

在HTML中，标签和属性不区分大小写，而属性值区分大小写。所以，在 CSS 中，标签选择器和属性选择器中的属性不区分大小写，而类选择器和ID选择器要区分大小写。

目前浏览器基本支持属性选择器中的属性值也不区分大小写（在 ] 前面写一个i），如果希望HTML中类名对大小写不敏感，可以这样写：

```CSS
[class~=VAL i] {}
```

#### 3.2 选择器命名的合法性

[W3C 2.1-4.1.3](https://www.w3.org/TR/2011/REC-CSS2-20110607/syndata.html#characters)
根据上述命名规范，所有UNICODE字符都是可以使用 \ 转义使用的，推荐使用的有以下几种：

1. 不合法 ACSCII 字符，如 !、"、#、$、%、^、&等

```CSS
/* .+foo */
.\2b foo {
  color: red;
}

/* .+foo */
.\00002b foo {
  color: red;
}

/* .+foo */
.\+foo {
  color: red;
}

/* .|foo */
.\|foo {
  color: red;
}

...
/* IE中不支持 \:，可用 \3a 代替 */
```

2. 中文字符和标点

```CSS
.我是foo {
  color: red;
}

.。foo {
  color: red;
}
```

3. emoji表情

```CSS
.😂 {
  color: red;
}
```

4. 短横线开头

```CSS
.-- {
  color: red;
}

.-a-b- {
  color: red;
}
```

#### 3.3 命名取舍

1. HTML语义化是为了让及其识别（如搜索引擎或屏幕阅读器），CSS选择器语义化是方便人识别，因此尽量使用短命名。
2. 单命名的优点是字符少，书写快，但容易出现冲突。
  + 多人合作、长期维护的项目，不要使用以常见单词命名的单命名选择器，通常冠以模块或场景名称前缀；
  + 第三方UI组件添加统一前缀；
  + 自主研发的项目，且维护时不直接加入第三方CSS，则公用结构、颜色等可以使用单命名；
  + 如果项目不需要长期维护和多人合作，务必添加统一的项目前缀，方便需要复用时能够直接复制使用。
  + 面向 CSS 属性的简写单命名是允许的。

  

```CSS
  .db {
    display: block;
  }

  .tac {
    text-align: center;
  }
```

#### 3.4 最佳实践

1. 不要使用ID选择器

ID选择器优先级太高，覆盖时只能再用ID选择器甚至行内式覆盖。如果一定要使用ID作为选择器标识，使用属性选择器，如：

```CSS
[id="csId"] {
  color: red;
}
```

2. 不要嵌套选择器

  + 渲染性能糟糕：标签选择器和过深的嵌套都会影响渲染性能。

```code
性能排序：ID选择器 > 类选择器 > 标签选择器 > * > 属性 > 部分伪类(如:checked)。
浏览器对于选择器的匹配规则是从右向左匹配，所以尽量将高性能的选择器写在后面。
性能问题对整个网页渲染速度的影响**并不大**。
```

  
  + 优先级混乱，维护困难。

  + 样式布局脆弱，层级越多，对HTML结构限定也就越多，需要修改时成本越高。

尽量全部使用无嵌套的纯类名选择器。
创建更多面向属性的命名，添加对于类名而不是新增属性来修改样式。
使用状态类名进行交互控制。

## 第 4 章

### 4.1 后代选择符（空格）

#### 4.1.1 错误认识-CSS中

```html
<div class="lightblue">
  <div class="darkblue">
    <p>1. 颜色是？</p>
  </div>
</div>
<div class="darkblue">
  <div class="lightblue">
    <p>2. 颜色是？</p>
  </div>
</div>
```

```CSS
.lightblue p {
  color: lightblue;
}

.darkblue p {
  color: darkblue;
}
```

1 和2 全部都是深蓝色。后代选择器对整个选择器的优先级与祖先元素的 DOM 层级没有任何关系，需要查看落地元素，此处即 **\<p\>** 标签，仅根据权重大小和“**后来居上**”原则判断。

##### 4.1.2 错误认识-JavaScript中

```javascript
< div id = "myId" >
  <
  div class = "lonely" > 单独节点 < /div> <
div class = "outer" >
  <
  div class = "inner" > 嵌套节点 < /div> < /
div > <
  /div>
```

使用 JavaScript 和后代选择器获取元素。

```javascript
document.querySelectorAll('#myId div div').length; // 1
document.querySelector('#myId').querySelectorAll('div div').length; // 3
```

**CSS 选择器是独立于整个页面的！**

```javascript
document.querySelector('#myId').querySelectorAll('div div');
```

传入 querySelector 和 querySelectorAll 方法的选择器都是全局范围。
上面代码的意思是：查询 #myId 元素的子元素，选择所有同时满足**整个页面**下 div div 选择器条件的 DOM 元素。虽然是链式调用，虽然调用方法的对象不同，但两个方法的选择器都是全局范围。
此时，可使用前面所述的局部作用伪类。

```javascript
document.querySelector('#myId').querySelectorAll(':scope div div'); // 1
```

### 4.2 子选择符（>）

#### 4.2.1 区别

略。

#### 4.2.2 适用场景

很明显，子选择符的范围一定比后代选择符的范围更窄，但这点性能对于后期可维护性来说微不足道，除非是为了**避免冲突**，尽量使用后代选择符而非子选择符。

1. 状态类名控制

```CSS
.active>.cs-module-x {
  display: block;
}
```

2. 标签受限（重复嵌套）

```CSS
.widget>li {}

.widget>li li {}
```

3. 层级位置与动态判断。例如某浮动组件平时以绝对或固定定位的形式作为 body 的子元素，但有时候需要其以静态布局嵌再页面的某个位置，可以借助子选择符对样式打补丁：

```CSS
:not(body)>.cs-date-panel-x {
  position: relative;
}
```

### 4.3 相邻兄弟选择符（+）

相邻兄弟选择符本身只会选择后面 **一个** 兄弟

#### 4.3.1 细节

**相邻兄弟选择符会忽略文本节点和注释节点**

#### 4.3.2 实现类似 :first-child 的效果

例如，如果希望除了第一个列表意外的其他列表都有 margin-top 属性值，可以这样写：

```CSS
.cs-li:not(:first-child) {
  margintop: 1em;
}

/* 兼容 IE8 浏览器 */
.cs-li {
  margin-top: 1em;
}

.cs-li:first-child {
  margin-top: 0;
}

/* 相邻兄弟选择符 */
.cs-li+.cs-li {
  margin-top: 1em;
}
```

且相邻兄弟选择符适用性比 :first-child 更广，例如：

```html
<div class="cs-g1">
  <h4>使用 :first-child 实现</h4>
  <p class="cs-li">列表内容 1</p>
  <p class="cs-li">列表内容 1</p>
  <p class="cs-li">列表内容 1</p>
</div>
<div class="cs-g2">
  <h4>使用相邻兄弟选择符实现</h4>
  <p class="cs-li">列表内容 1</p>
  <p class="cs-li">列表内容 1</p>
  <p class="cs-li">列表内容 1</p>
</div>
```

```CSS
.cs-g1 .cs-li:not(:first-child) {
  color: skyblue;
}

.cs-g2 .cs-li+.cs-li {
  color: skyblue;
}
```

使用 :first-child 实现的第一项列表内容样式无法正确呈现。

##### 4.3.3 高级选择器技术核心

当聚焦输入框时，如果希望后面的提示文字显示，则可以借助相邻兄弟选择符实现，只需要把提示文字预先放在输入框的后面。

```html
用户名：<input><span class="cs-tips">不超过 10 个字符</span>
```

```CSS
.cs-tips {
  color: gray;
  margin-left: 15px;
  position: absolute;
  visibility: hidden;
}

:focus+.cs-tips {
  visibility: visible;
}
```

### 4.4 兄弟选择符（~）

#### 4.4.1 与相邻兄弟选择符的区别

略。

#### 4.4.2 为什么没有前面兄弟选择符

浏览器解析 HTML 文档是从前往后，由外及里进行的，所有时常会看到页面先出现头部然后再出现主体内容的情况。
如果 CSS 支持了前面兄弟选择符或者父元素选择符，那就必须要等页面所有子元素加载完毕才能渲染 HTML 文档。如果真的实现了支持，网页呈现速度必然会大大减慢，出现长时间的白板。
如果强制采取加载到那里就渲染到哪里的策略，那么会出现前面已经渲染好的样式会突然编程另外一个样式的情况。
规范文档中有一个伪类 :has 可以实现类似父选择器和前面选择器的效果，但依然没有任何浏览器实现相关功能。

#### 4.4.3 如何实现前面兄弟选择符的效果

兄弟选择符只能选择 **后面** 的元素，但是这个“后面”仅仅指代码层面的后面，而不是视觉层面的后面。也就是说，要实现前面兄弟选择符的效果，可以把这个“前面的元素”的结构代码依然放在后面，并且用相邻兄弟选择符或者兄弟选择符调整样式，在视觉上将它呈现在前面就可以了。

```html
<div class="cs-flex cs-float cs-absolute cs-direction cs-margin">
  <input class="cs-input"><label class="cs-label">用户名：</label>
</div>
```

实现方法有：

1. Flex布局。

```CSS
.cs-flex {
  display: inline-flex;
  flex-direction: row-reverse;
}

.cs-input {
  width: 200px;
}

.cs-label {
  width: 64px;
}

:focus~.cs-label {
  color: darkblue;
  text-shadow: 0 0 1px;
}
```

```code
唯一问题：不兼容 IE8.
```

2. float 浮动。

```CSS
.cs-float {
  width: 264px;
}

.cs-input {
  float: right;
  width: 200px;
}

.cs-label {
  display: block;
  overflow: hidden;
}

:focus~.cs-label {
  color: darkblue;
  text-shadow: 0 0 1px;
}
```

```code
优点：兼容性极佳。
缺点：容器宽度需要根据子元素宽度计算（如果不需兼容，可以配合 calc() 计算）；不能实现多个元素效果。
```

3. absolute 绝对定位。

```CSS
.cs-absolute {
  width: 264px;
  position: relative;
}

.cs-input {
  width: 200px;
  margin-left: 64px
}

.cs-label {
  position: absolute;
  left: 0;
}

:focus~.cs-label {
  color: darkblue;
  text-shadow: 0 0 1px;
}
```

```code
缺点：元素较多时，控制成本较高。
```

4. direction 属性实现，改变文档流顺序。

```CSS
/* 父盒子水平文档流顺序改为从右往左 */
.cs-direction {
  direction: rtl;
}

/* 子盒子及其内部水平文档流顺序还原为从左往右 */
.cs-direction .cs-label,
.cs-direction .cs-input {
  direction: ltr;
}

.cs-label {
  display: inline-block;
  /* 必须 */
}

:focus~.cs-label {
  color: darkblue;
  text-shadow: 0 0 1px;
}
```

```code
唯一缺点：子盒子必须是内联元素。
```

5. margin 定位

```CSS
.cs-margin {
  margin-left: 64px;
}

.cs-input {
  width: 200px;
}

.cs-label {
  margin-left: -264px;
}

:focus~.cs-label {
  color: darkblue;
  text-shadow: 0 0 1px;
}
```

```code
缺点：宽度和外边距需要计算。
```

### 4.5 列选择符（||）

目前的浏览器对此选择符的兼容性还不太好。
Tabel 布局和 Grid 布局中都有列的概念，有时候我们希望控制整列的样式，可以借助 :nth-col() 或者 :nth-last-col() 伪类实现，但目前浏览器的支持度也不太好；另外可以借助原生 Table 布局中的 **\<colgroup\>** 和 **\<col\>** 元素实现，兼容性很好，具体用法可以百度。
但有时候需要设置样式的单元格存在跨列，则 **\<col\>** 元素会忽略这些跨列单元格的样式，而列选择符就是用来选中某列，只要某单元格属于某列，则样式就可以生效。

## 第 5 章 元素（标签）选择器

### 5.1 级联用法

元素选择器的级联语法和其他选择器的级联语法的区别：

1. 元素选择器是唯一不能重复自身的选择器。
2. 级联使用的时候元素选择器必须写在最前面。

但仍然有办法无副作用地提高选择器优先级：

1. 借助根节点

```CSS
html foo,
body foo {}
```

2. 借助 :not() 伪类，括号里写任意不一样的标签名称即可

```CSS
foo:not(not-foo),
foo:not(a),
foo:not(_) {}
```

### 5.2 细节

#### 5.2.1 混合其他选择器的优化

```CSS
input[type="radio"] {}

a[href^="http"] {}

img[alt] {}

div#cs-some-id {}
```

以上含有某些标签的特有属性选择器的，都可以省略掉标签选择器

#### 5.2.2 自定义标签

```html
<x-element>自定义元素</x-element>
<!-- 如果需要兼容 IE8 浏览器，需要加上下面这行代码 -->
<script>
  document.createElement('x-element')
</script>
```

```CSS
x-element {
  color: red;
}
```

### 5.3 特殊标签选择器：通配符

```CSS
*,
::before,
::after {
  box-sizing: border-box;
}
```

大多数时候可以省略通配符，只有单独使用的时候，才需要把 * 字符呈现出来。

```CSS
body>* {}
```

## 第 6 章 属性选择器

正是文档中，类选择器和 ID 选择器都属于属性选择器（本质是 class 和 id 属性），平时提到的属性选择器实际上指的是“属性值匹配选择器”。

### 6.1 ID 选择器和类选择器

基础略。

无论是使用 JsvaScript 的选择器 API 获取元素，还是使用 CSS 的 ID 选择器设置样式，对于 ID，其在语义上是不能重复的，但实际开发的时候，语义重复也是可以的，并不影响功能。

```html
<button id="cs-button">按钮 1</button>
<button id="cs-button">按钮 2</button>
```

```javascript
document.querySelectorAll('#cs-button').length; // 2
```

```CSS
/* 可以同时设置两个按钮的样式 */
#cs-button {}
```

但是**并不推荐这样做**。

#### 6.2 属性值直接匹配选择器

##### 6.2.1 细节

1. [attr]

[attr] 表示只要包含指定的属性就匹配，尤其适用于一些 HTML 布尔属性。

可以应用于 disabled 属性，但是建议不要用于 checked 属性。因为虽然 checked 属性选择器可以生效，但是表单控件元素在 checked 状态变化时并不会同步修改 checked 属性的值，而 disabled 属性会同步。要么使用 :checked 伪类实现，要么在状态变化时手动更新 checked 属性。

自定义属性也是受支持的。

2. [attr="val"]

属性值完全匹配选择器，属性值不区分单双引号，且可以省略，但如果属性值包含空格，则需要转义：

```html
<button class="cs-button cs-button-primary">主按钮</button>
```

```CSS
[class=cs-button\0020cs-button-primary] [class="cs-button cs-button-primary"]
```

[type=email]等选择器使用有风险（仅针对 IE9 及其以上版本浏览器的兼容模式，IE8 已修复，不必深究）。
表单控件使用类似 [value="20"] 的选择器，同样存在属性不更新导致样式失效的问题。

3. [attr~="val"]

属性值单词完全匹配选择器，用于匹配被空格分隔的关键词，这里的属性值不能带有空格。

```html
<a href rel="nofollow noopener 我 val      ue">链接</a>
<a href rel="value val-ue">链接</a>
```

```CSS
[ref~="nofollow"] {}

/* 匹配第一个 */
[ref~="noopener"] {}

/* 匹配第一个 */
[ref~=""] {}

/* 无匹配 */
[ref~="val"] {}

/* 匹配第一个，不匹配第二个 */
[ref~=帅] {}

/* 匹配第一个 */
```

*适用场景*

非常适合包含多种组合属性值的场景。例如，某元素共有 9 种定位控制：

```html
<div data-align="left top"></div>
<div data-align="top"></div>
<div data-align="right top"></div>
<div data-align="right"></div>
<div data-align="right bottom"></div>
<div data-align="bottom"></div>
<div data-align="left bottom"></div>
<div data-align="left"></div>
<div data-align="center"></div>
```

```CSS
[data-align] {
  left: 50%;
  top: 50%;
}

[data-align~="top"] {
  top: 0;
}

[data-align~="right"] {
  right: 0;
}

[data-align~="bottom"] {
  bottom: 0
}

[data-align~="left"] {
  left: 0;
}
```

如果使用类选择器，加上模块名，会使 class 非常长而不易读。

4. [attr|="val"]

属性值起始片段完全匹配选择器。表示具有 attr 属性的元素，其值要么正好是 val，要么以 val 加短横线 -(U+002D)开头。

```html
<div attr="val"></div>
<div attr="val-ue"></div>
<div attr="val-ue bar"></div>
<!-- 不匹配 -->
<div attr="value"></div>
<!-- 不匹配 -->
<div attr="val bar"></div>
<!-- 不匹配 -->
<div attr="bar val-ue"></div>
```

此选择器的设计初衷是子语言匹配，比如zh-CN、zh-HK、en-US、en-UK等。但大多数 Web 开发都不涉及多语言，而且可以使用 :lang 伪类，此选择器更多用于 hreflang 属性值匹配。

##### 6.2.2 AMCSS 开发模式

基于属性控制的样式开发模式，可在网络中查阅相关资料。

##### 6.3 属性值正则匹配选择器

##### 6.3.1 细节

```CSS
[attr^="val"] [attr$="val"] [attr*="val"]
```

^、$、* 都是正则表达式中的特殊标识符，分别表示前匹配、后匹配和任意匹配。

前匹配多用于判断 **\<a\>** 元素的链接地址，显示对应的小图标等。
后匹配多用于判断 **\<a\>** 元素链接的文件类型。
任意匹配多用于判断 **\<a\>** 元素的链接是否是外网地址，另外常用于匹配 style 属性值。

##### 6.3.2 搜索过滤技术

```html
<input type="search" placeholder="输入城市名称或拼音" />
<ul>
  <li data-search="重庆市 chongqing">重庆市</li>
  <li data-search="哈尔滨市 haerbin">哈尔滨市</li>
  <li data-search="长春市 changchun">长春市</li>
  ...
</ul>
```

```javascript
var eleStyle = document.createElement('style');
document.head.appendChild(eleStyle);

input.addEventListener('input', function() {
  var value = this.value.trim();
  eleStyle.innerHTML = value && '[data-search]:not([data-search*=")' + value + '"]) { display: none; }'
});
```

#### 6.4 忽略属性值大小写

无论是普通或正则属性选择器，都可以在 ] 前加上 i 来忽略属性值大小写判断。

## 第 7 章 用户行为伪类

### 7.1 手型经过伪类 :hover

最早只能用在 **\<a\>** 元素上，目前可用在所有 HTML 元素上，包括自定义元素。
不适用于移动端，虽然能触发，但体验奇怪。

#### 7.1.1 体验优化与 :hover 延时

表格最右侧的操作栏按钮经常赋予一个漂浮的 tips 文本标签，但常常在高度不够时覆盖掉上一行的按钮，可以使 visibility 属性延时生效来优化体验。

```CSS
.icon-delete::before,
.icon-delete::after {
  transition: visibility 0s .2s;
  visibility: hidden;
}

.cion-delete:hover::before,
.cion-delete:hover::after {
  visibility: visible;
}
```

#### 7.1.2 非子元素的 :hover 显示

如果要实现一个鼠标警告链接预览图片的功能，不方便嵌套图片标签，则可以用相邻兄弟选择符来实现。如果标签间有空隙导致鼠标移动时图片立刻消失，可以给图片的 visibility 属性加上延时。

#### 7.1.3 纯 :hover 显示浮层的体验问题

具有交互行为的设计，一定 **不要只使用** :hover 伪类实现。虽然方便，但是如果用户的鼠标坏了，或者是触屏设备等不具有鼠标的场景，则此功能会完全瘫痪。

对此，可以使用 :focus 伪类和 :focus-within 整体焦点伪类增强体验，如果不需要兼容，建议只使用 :focus-within。
想要好的用户体验，仅靠 CSS 代码实现的效果是不够的。

### 7.2 激活状态伪类 :active

#### 7.2.1 :active 伪类概述

可用于设置元素激活状态的样式，可通过点击鼠标主键、手指或触控笔点击触摸屏触发。具体表现为：点击按下触发 :active 伪类样式，点击抬起取消 :active 伪类样式的应用。支持任意 HTML 元素，包括非控件元素和自定义元素。

但实践上 :active 伪类并没有理论上那么完美，包括一下几点：

1. IE 浏览器下 :active 样式的应用是无法冒泡的；

2. 在 IE 浏览器下，**\<html\>**、**\<body\>**元素上应用 :active 伪类设置背景色后，背景色是无法还原的（其他一些属性可以还原）；

3. 移动端 Safari 浏览器下，:active 伪类默认是无效的，需要设置任意的 touch 事件才支持；

4. 键盘访问无法触发 :active 伪类。

:active 伪类的主要作用是反馈点击交互，且其只作用于鼠标处于按下状态的一段时间，所以不适合用来做复杂交互。

#### 7.2.2 按钮的通用 :active 样式技巧

***更适用于移动端开发***，因为桌面端可以通过 :hover 反馈状态变化。

1. 使用 box-shadow 内阴影，例如：

```CSS
[href]:active,
button:active {
  box-shadow: inset 0 0 0 999px rgb(0, 0, 0, .05);
}
```

  优点：可以兼容到 IE9 浏览器；
  缺点：对非对称闭合元素无效，比如 **\<input\>** 按钮。

2. 线性渐变

```CSS
[href]:active,
button:active,
[type=reset]:active,
[type=button]:active,
[type=submit]:active {
  background-image: linear-gradient(rgba(0, 0, 0, .05), rgba(0, 0, 0, .05));
}
```

```code
优点：对非对称闭合元素也有效；
缺点：只兼容到 IE10 浏览器
```

3. 外边框

适合移动端的图片元素，且不包含复杂信息（outline 模拟的反馈浮层位于元素上方，且可以被绝对定位的子元素穿透）。

```html
<a href><img src="1.jpg"></a>
```

```CSS
[href]>img:only-child:active {
  outline: 999px solid rgba(0, 0, 0, .05);
  outline-offset: -999px;
  -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%);
  clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%);
}
```

#### 7.2.3 :active 伪类与 CSS 数据上报

```CSS
.button-1:active::after {
  content: url(./pixel.gif?action=click&id=button1);
  display: none;
}
```

### 7.3 焦点伪类 :focus

#### 7.3.1 匹配机制

:focus 伪类默认只能匹配特定的元素，包括：
* 非 disabled 状态的表单元素
* 包含 href 属性的 **\<a\>** 元素
* **\<area\>** 元素，仅部分属性生效
* HTML5 中的 **\<summary\>** 元素
* 设置了 HTML contenteditable 属性的普通元素
* 设置了 HTML tabindex 属性的普通元素

如果期望元素可以被 Tab 键索引，且被点击的时候可以触发 :focus 伪类样式，则使用 tabindex="0"；如果不期望被索引，且只在它被点击的时候触发 :focus 伪类样式，则使用 tabindex="-1"。对于普通元素，没有使用**自然数**作为 tabindex 属性值的场景。

可以利用这种特性实现任意元素的点击下拉效果。

```html
<img src="icon-qrcode.svg" tabindex="0">
<img class="img-qrcode" src="qrcode.png">
```

```CSS
.img-qrcode {
  position: absolute;
  display: none;
}

:focus+.img-qrcode {
  display: inline;
}
```

如上，点击第一个小图标，则大的二维码图片显示，点击空白处，图片又会隐藏。

但这种方法并不完美，在 iOS Safari 浏览器下，元素一旦处于 focus 状态，除非点击其他可聚焦元素来转义 focus 焦点，否则这个元素会一直保持 focus 状态，其它浏览器无此问题。解决方案需要给祖先容器元素（非 body）设置 tabindex="-1"，同时取消目标元素的 outline 样式即可。

但是一旦其他交互效果导致失焦或焦点转移，都会导致下拉浮层的小时。

另外，一个页面永远最多有一个焦点元素，也就意味着同时最多只有一个元素能响应 :focus 伪类样式。

#### 7.3.2 :focus 伪类与 outline

1. 禁止使用下列代码：

```CSS
* {
  outline: 0 none;
}

a {
  outline: 0 none;
}
```

本来是为了取消 IE 浏览器时代点击链接或按钮会出现虚框轮廓，但这样会妨碍键盘访问，完全无法判断目标元素。

2. 模拟原生的 focus 轮廓

```CSS
:focus {
  outline: 1px dotted;
  outline: 5px auto -webkit-focus-ring-color;
}
```

#### 7.3.3 CSS :focus 伪类与键盘无障碍访问

1. 为什么不建议使用 span 或 div 按钮

原生的 **\<button\>** 元素可以触发表单提交行为，原生支持 Enter 键，且原生可以被键盘操作 focus，保证无障碍访问。
其他元素需要设置 tabindex="0" 支持 Tab 键索引，role="button" 支持屏幕阅读器识别等。如果确实不能使用 button 元素，可以使用 **\<label\>** 元素模拟，通过 for 属性关联。

```html
<input id="submit" type="submit">
<!-- 更改 label 的样式模拟按钮 -->
<label class="button" for="submit">提交</label>
```

2. 模拟表单元素的键盘可访问性

radio、checkbox、range 类型的 **\<input\>** 元素常常需要重新设计，此时可如上利用 label 来实现 UI 效果，且隐藏原 input 元素，其中有两个关键点：

**（1）**使用 opacity: 0 隐藏或者 clip-path 剪裁，不要使用 visibility: hidden 或者 display: none 进行隐藏，因为这样是无法被键盘聚焦的。
**（2）**在原始 input 元素聚焦时高亮显示自定义的输入框元素。。

3. 容易被忽略的鼠标经过行为的键盘可访问性

* 按钮 hover 时显示 tips 浮层
* 列表中默认隐藏，当鼠标经过某行时才显示对应按钮（使用 opacity: 0隐藏）

### 7.4 整体焦点伪类 :focus-within

#### 7.4.1 区别

:focus 伪类样式只在当前元素本身处于聚焦状态的时候才匹配，而 :focus-within 伪类样式在当前元素或当前元素的子元素处于聚焦状态的时候都会匹配。

#### 7.4.2 :focus-within 实现无障碍访问的下拉列表

与用 :focus 实现的原理类似，略。

#### 7.5 键盘焦点伪类 :focus-visible

:focus-visible 伪类匹配的场景是：元素聚焦，同时浏览器认为聚焦轮廓应该显示。在 Chrome 中表现为仅键盘聚焦时此伪类才会生效，鼠标点击时不会触发，可用于设置了背景的 **\<button\>** 按钮、HTML5 中的 **\<summary\>** 元素、设置了 HTML tabindex 属性的元素。但其他浏览器不存在此问题，也尚未实现此伪类。

## 第 8 章

### 8.1 链接历史伪类 :link 和 :visited

#### 8.1.1 深入理解 :link

:link 用于匹配页面上 href 链接没有被访问过的 **\<a\>** 元素。

```CSS
a:link {
  color: skyblue
}

;

a:visited {
  color: lightskyblue;
}
```

样式书写顺序：“love-hate”
:link -→ :visited -→ :hover -→ :active，首字母连起来是LVHA，取自love-hate.

目前直接用 a 标签比用 :link 伪类更有市场，但后者可以用于识别“真”链接。

```html
<a href>链接</a>
<!-- 对 :link 伪类样式无响应 -->
<a name="example">非链接</a>
```

这个功能点也可以用 [href] 来代替。

#### 8.1.2 怪癖最多的CSS伪类:visited

1. 支持的CSS属性有限

:visited伪类选择器支持的 CSS 很有限，目前仅支持下面这些 CSS: **color, background-color, border-color, border-bottom-color, border-left-color, border-right-color, border-top-color, column-rule-color, outline-color**。类似 ::before 和 ::after 这些伪元素则不支持。

```html
<a href="">文字<small></small></a>
```

```css
small {
  position: absolute;
  color: white;
}

/* 这里设置coor: transparent无效 */
small::after {
  content: 'visited';
}

a:visited small {
  color: lightakyblue;
}
```

2. 没有半透明

使用 :visited 伪类选择器控制颜色时，虽然在语法上它支持半透明色，但是在表现上，要么纯色，要么全透明。

3. 只能重置,不能凭空设置

对于下面这段Css, 访问过的 a 元素不会有背景色。

```css
a {
  color: blue;
}

/* 改为下面这样就会显示背景色 */
/* a { color:blue; background-color: white; } */
a:visited {
  color: red;
  background-color: gray;
}
```

4. 无法获取: visited设置和呈现的色值

当文字颜色值表现为 :visited 选择器设置的颜色值时，我们使用 JavaScript 的 getComputedstyle() 方法将无法获取到这个颜色值。

### 8.2 超链接伪类:any-link

:any-link伪类有如下两大特性：

* 匹配所有设置了href属性的链接元素, 包括**&#60; a&#62; &#60; link&#62; **和**&#60; area&#62; **这3种元素; 
* 匹配所有匹配:link伪类或者:visited伪类的元素（避免默认已访问样式的干扰，直接覆盖）。

不兼容 IE 浏览器。

### 8.3 目标伪类:target

#### 8.3.1 :target与锚点

匹配 URL 锚点对应的元素（多个相同id时仅匹配第一个节点）。
如果匹配锚点的元素是 display:none，则所有浏览器不会触发任何滚动，但是 display:none 元素依然匹配:target伪类。

#### 8.3.2 :target交互布局技术简介

1. 展开与收起效果

例如, 一篇文章只显示了部分内容, 需要点击“阅读更多”才显示剩余内容，HTML如下：

```html
文章内容,文章内容,文章内容,文章内容,文章内容,文章内容,文章内容……

<div id="articleMore" hidden></div>

<a href="#articleMore" class="cs-button" data-open="true">阅读更多</a>

<p class="cs-more-p">更多文章内容,更多文章内容,更多文章内容,更多文章内容。</p>

<a href="##" class="cs-button" data-open="false">收起</a>
```

这里依次出现了以下4个标签元素：

div#articleMore元素是一直隐藏的锚链元素，用来匹配:target伪类；
a[data-open="true"]是“阅读更多”按钮，点击地址栏中的URL地址，锚点值会变成#articleMore，从而触发:target伪类的匹配；

p.cs-more-p是默认隐藏的更多的文章内容；

a[data-open="false"]是收起按钮，点击后将重置锚点值，页面的所有元素都不会匹配:target伪类。

相关CSS如下:

```css
/* 默认“更多文章内容”和“收起”按钮隐藏 */
.cs-more-P,
[data-open=false] {
  display: none;
}

/* 匹配后“阅读更多”按钮隐藏 */
:target~[data-open=true] {
  display: none;
}

/* 匹配后“更多文章内容”和“收起”按钮显示 */
:target~.cs-more-p,
:target~[data-open=false] {
  display: block;
}
```

也可用于弹窗显隐。

2. 选项卡效果

HTML如下:

```html
<div class="cs-tab-x">
  <!--锚链元素-->
  <i id="tabPanel2" class="cs-tab-anchor-2" hidden></i>
  <i id="tabPanel3" class="cs-tab-anchor-3" hidden></i>

  <!--以下html为标准选项卡结构-->
  <div class="cs-tab">
    <a href="#tabPanel1" class="cs-tab-li">选项卡1</a>
    <a href="#tabPanel2" class="cs-tab-1i">选项卡2</a>
    <a href="#tabPanel3" class="cs-tab-li">选项卡3</a>
  </div>
  <div class="cs-panel">
    <div class=cs-panel-i">面板内容1</div>
    <div class=cs-panel-i">面板内容2</div>
    <div class=cs-panel-i">面板内容3</div>
  </div>
</div>
```

相关CSS代码如下:

```css
/* 默认选项卡按钮样式 */
.cs-tab-li {
  display: inline-block;
  background-color: #f0f0f0;
  color: #333;
  padding: 5px 10px;
}

/* 选中后的选项卡按钮样式 */
.cs-tab-anchor-2:not(:target)+ :not(:target)~.cs-tab .cs-tab-li:first-child,
.cs-tab-anchor-2:target~.cs-tab .cs-tab-li:nth-of-type(2),
.cs-tab-anchor-3:target~.cs-tab .cs-tab-11:nth-of-type(3) {
  background-color: deepskyblue;
  color: #fff;
}

/* 默认选项面板样式 */
.cs-panel-li {
  display: none;
  padding: 20px;
  border: 1px solid #ccc;
}

/* 选中的选项面板显示 */
.cs-tab-anchor-2:not(:target)+ :not(:target)~.cs-panel .cs-panel-li:first-child,
.cs-tab-anchor-2:target~.cs-panel .cs-panel-li:nth-of-type(2) .cs-tab-anchor-3:target~.cs-panel .cs-panel-l1:nth-of-type(3) {
  display: block;
}
```

3. 双管齐下

:target伪类交互技术也不是完美的，一是它对DOM结构有要求，锚链元素需要放在前面；二是它的布局效果并不稳定，由于URL地址中的锚点只有一个，因此，一旦页面其他什么方有一个锚链，如href的属性值是###，用户一点击，原本选中的第二个选项卡就会莫名其妙地切换到第一个选项上去，这可能并不是用户希望看到的。

因此，在实际开发中，如果对项目要求很高，推荐使用双管齐下的实践策略，具体如下：

* 仍按照:target伪类交互技术实现，实现的时候与一个类名标志量关联。
* 正常实现选项卡交互，当成功绑定后，移除类名标志量，交互由 JavaScript 接手。

### 8.4 目标容器伪类:target-within

可匹配触发:target伪类的元素及其父级元素，但目前支持度不好。

## 第9章 输入伪类

### 9.1 输入控件状态

#### 9.1.1 可用状态与禁用状态伪类:enabled和:disabled

1. 与[disabled]效果一样，单独设计的原因参考[9-2-1](#9.2.1-选中选项伪类:checked)

2. 若干细节

* :enabled和:disabled伪类一般是完全对立的，但在Chrome浏览器下，带有href属性的& a 元素可以匹配:enabled伪类，但没有任何 a 元素匹配:disabled伪类，所以尽量避免使用裸露的:enabled伪类。
* 对于 select 下拉框元素，无论是 select 元素自身，还是后代 option 元素，都能匹配:enabled伪类和:disabled伪类，所有浏览器都匹配。
* 在 IE 浏览器下 fieldset 元素并不支持:enabled伪类与:disabled伪类，其他浏览器没有这个问题。因此，如果使用 fieldset 元素一次性禁用所有表单元素 就不能通过:disabled伪类识别（如果要兼容IE），可以使用 fieldset[disabled] 选择器进行匹配。
* 设置 contenteditable="true" 的元素虽然也有输入特征，但是并不能匹配:enabled伪类，所有浏览器都不匹配。同样，设置tabindex属性的元素也不能匹配:enabled伪类。
* 元素设置 visibility: hidden 或者 display: none 依然能够匹配:enabled伪类和:disabled伪类。

3. :enabled伪类和:disabled伪类的实际应用

* 使用 querySelectorAll 匹配可用元素
* 禁用 button 按钮。对于禁用状态，很多人会用 pointer-events:none 来控制，虽然点击它确实无效，但是键盘Tab依然可以访问它, ，按回车键也依然可以触发点击事件，用这种方法实现的其实是一个伪禁用。同时，设了 pointer-events: none元素无法显示 title 提示。

#### 9.1.2 读写特性伪类:read-only和:read-write

用于匹配输入框元素是否只读，还是可读可写，只作用于 input 和 textarea 这两个元素。

**readonly 和 disabled的区别**

设置 readonly 的输入框不能输入内容，但它可以被表单提交；设置 disabled 的输入框不能输入内容，也不能被表单提交。readonly输入框和普通输入框的样式类似，但是浏览器会将设置了 disabled 的输入框中的文字置灰来加以区分。

#### 9.1.3 占位符显示伪类:placeholder-shown

当输入框的 placeholder 内容显示的时候，匹配该输入框。

1. 实现 Material Design 风格占位符交互效果

```html
<div class="input-fill-x">
  <input class="input-fill" placeholder="邮箱">
  <label class="input-1abel">邮箱</label>
</div>
```

```css
/* 默认 placeholder颜色透明不可见 */
.input-fill:placeholder-shown::placeholder {
  color: transparent;
}

/* 用下面的 .input--label 元素代替浏览器原生的占位符成为我们肉眼看到的占位符 */
.input-fi11-x {
  position: relative;
}

.input-label {
  position: absolute;
  left: 16px;
  top: 14px;
  pointer-events: none:
}

/* 在输入框聚焦以及占位符不显示的时候对 <label> 元素进行重定位 */
.input-fill:not(:placeholder-shown)~.input-label,
.input-fill:focus~.input-label {
  transform: scale(.75) translate(0, -32px);
}
```

2. :placeholder-shown与空值判断

可以借助 :placeholder-shown 伪类来判断一个输入框中是否有值，控制提示信息的显隐。

#### 9.1.4 默认选项伪类:default

* 渲染优先度高，不易受 JavaScript 影响。
* 如果 option 没有设置 selected 属性，浏览器会默认呈现第一个 option，但第一个 option不会匹配 :default 伪类。
* 可应用于支付场景的支付方式“推荐标记”，更改默认选项后不用更改文本。

### 9.2 输入值状态

#### 9.2.1 选中选项伪类:checked

1. 为何不直接使用 [checked] 属性选择器

* :checked 只能匹配标准表单控件元素，不能匹配其他普通元素，即使这个普通元素设置了 checked 属性。但是 [checked] 属性选择器却可以与任意元素匹配。
* [checked] 属性的变化并非实时的。
* 伪类可以正确匹配从祖先元素那里继承过来的状态，但是属性选择器却不可以。

2. 可以用类似 :target 伪类的思路实现显隐和选项卡，但因为不易维护和没有记忆功能，最佳实现方式是 **details + summary 元素技术** > **JavaScript** > **单复选框显隐**

可以应用的最佳实践案例有：

1. 自定义单复选框样式覆盖
2. 复选框样式覆盖实现开关效果
3. 标签/列表/素材选择的样式实现

#### 9.2.2 不确定值伪类:indeterminate

**没有原生的 HTML 属性可以设置半选状态，半选状态只能通过 JavaScript 进行设置。**

可以匹配单选框、复选框和进度条元素 progress。

1. 单选框

对于单选框元素，当所有 name 属性值一样的单选框都没有被选中的时候会匹配 :indeterminate 伪类；如果单选框元素没有设置 name 属性值，则其自身没有被选中的时也会匹配 :indeterminate 伪类。

可以用了提示用户尚未选择任何单选项，但不兼容 IE 浏览器。

2. progress 元素

当没有设置值的时候，会匹配 :indeterminate 伪类。

### 9.3 输入值验证

此节的伪类都是与表单元素的验证相关，配合 required 和 pattern 等验证属性使用，语义化较好，更多细节可以查阅 MDN。

1. 有效性验证伪类 :valid 和 :invalid
2. 范围验证伪类 :in-range 和 :out-of-range（是否超过 min 和 max 的范围）
3. 可选性伪类 :required 和 :optional（匹配是/否设置了 required 属性的元素）

***注意：***对于 :invalid 伪类，只要其中一个单选框设置了 requlred 属性，整个单选框组中的所有单选框元素都会匹配 :invalid 伪类，这会导致同时验证通过或验证不通过；但是，如果是 :required 伪类，则只会匹配设置了 required 属性的单选框元素。

*实现 必须/可选 标记*

![avatar](/assets/images/notes/伪类实现必选标记.png)

首先是HTML部分，和传统实现不同，我们需要把标题元素放在表单元素的后面，这样才使用兄弟选择符进行控制。

```html
<form>
  <fieldset>
    <legend>问卷调查</legend>
    <oi class="cs-ques-ol">
      <li class="cs-ques-li">
        <input type="radio" name="ques1" required>1-3年
        <input type="radio" name="ques">3-5年
        <input type="radio" name="ques1">5年以上
        <!-- 标题放在后面 -->
        <h4 class="cs-caption">你从事前端几年了?</h4>
      </li>
      ...
      <li class="cs-ques-li">
        <textarea></textarea>
        <!-- 标题放在后面 -->
        <h4 class="cs-caption">有什么其他想说的?</h4>
      </li>
      </ol>
      <p><input type="submit" value="提交"></p>
  </fieldset>
</form>
```

让后面的 .cs-caption 元素显示在上方：

```css
.cs-ques-li {
  display: table;
  width: 100%;
}

.cs-caption {
  display: table-caption;
  /* 标题显示在上方 */
  caption-side: top:
}
```

由于 li 元素设置了 display: table，重置了浏览器内置的 display: list-item，因此，列表前面的数字序号就无法显示，但没关系，我们可以借助 CSS 计数器重现序号匹配：

```css
.cs-ques-ol {
  counter-reset: quesIndex;
}

.cs-ques-li::before {
  counter-increment: quesIndex;
  content: counter(quesIndex) ".";
  /* 序号定位 */
  position: absolute;
  top: -.75em;
  margin: 0 0 0 -20px;
}
```

基于 :optional 伪类和 :required 伪类在 .cs-caption 元素最后标记可行性:

```css
:optional~.cs-caption::after {
  content: "（可选）";
  color: gray;
}

:required~.cs-caption::after {
  content: "（必选）";
  color: red;
}
```

以后要想修改可选性，只需要修改表单元素的 required 属性即可。

4. 用户交互伪类 :user-invalid 和空值伪类 :blank

:user-invalid 伪类用于匹配用户输入不正确的元素，但只有在用户与它进行了显变之后才进行匹配。:user-invalid 伪类必须在用户尝试提交表单和用户再次与表单元素交互之前匹配。目前浏览器实现存疑，实际开发请使用 :valid 类和 JavaScript 代码配合实现。
:blank伪类的规范也是多变的，一开始是可以匹配空标签元素（可以有空格），现在变成匹配没有输入值的表单元素。如果想要配空单元素，请使用 :placeholder-shown 伪类代替（设置 placeholder 属性值为空格）。

## 第10章 树结构伪类

### 10.1 :root伪类

:root伪类表示文档根元素。

#### 10.1.1 :root伪类和 html 元素

在 XHTML 或者 HTML 页面中，:root伪类表示的就是 html 元素。

:root伪类的优先级更高，毕竟伪类的优先级比标签选择器的优级要高一个层级；其次，对于:root，IE9及以上版本的浏览器才支持，它的兼容性要逊于 html 标签选择器；最后，:root指所有XML格式文档的根元素，XHTML文档只是其中一种。例如, 在SVG中，:root就不等同于html标签了，而是其他标签。

在 Shadow DOM 中虽然也有根的概念（称为shadowRoot），但并不能匹配 :root 伪类，应该使用专门为此场景设计的 :host 伪类。

#### 10.1.2 :root伪类的应用场景

由于 html 标签选择器的兼容性更好，优先级更低，因此日常开发中没有必要使用 :root 伪类，直接使用 html 标签选择器即可。

但下面这两个开发场景则更推荐使用 :root 伪类：

1. 滚动条出现页面不跳动

桌面端网页的主体内容多采用水平居中布局，类似下面这样(取自2019年的淘宝首页)

```css
.layer {
  width: 1190px;
  margin: 0 auto;
}

页面加载或者交互变化导致页面高度超过一屏的时候，页面就会有一个从无滚动条到有滚动条的变化过程。滚动条的出现必然导致页面的可用宽度变小，需要重新计算主体模块的居中定位，导致内容发生偏移，页面会突然跳动，体验很不好。 常见做法是下面这样的： ​``` css

/* 会让高度不足一屏的页面的右侧也显示滚动条的轨道，并不完美 */
html {
  overflow-y: scroll;
}

/* 外部再嵌套一层 div 元素，再设置 */
.layer-outer {
  margin-left: calc(100vw - 100%);
  /* 或者 */
  padding-left: calc(100vw - 100%);
}

/* 上面这种方法还是有瑕疵，当浏览器宽度比较小的时候，左侧留的白明显比右边多，可以通过查询语句进行优化 */
@media screen and (min-width: 1190px) {
  .layer-outer {
    margin-left: calc(100vw - 100%);
  }
}

/* 这种方法的另外一个不足就是需要调整HTML结构，修改成本较高 */

完美方案：

/* IE8 */
html {
  overflow-y: scroll;
}

/* IE9+ */
:root {
  overflow-x: hidden;
}

:root body {
  position: absolute;
  width: 100vw;
  overflow: hidden;
}
```

2. CSS变量

对于这些变量，业界约定俗成，都将它们写在 :root 伪类中，html选择器负责样式，:root伪类负责变量，可读性更好。

### 10.2 :empty伪类

1. 可以匹配空标签元素
2. 可以匹配前后闭合的替换元素，如 button 元素和 textarea 元素

*在IE浏览器下，如果输入文字内容，则IE浏览器认为 textarea 元素井非空标签，不会匹配 :empty 伪类。其次，当 textarea 元素的 placeholder 属性值显示的时候，IE浏览器也不会匹配 :empty 伪类（此缺陷可以利用，但业务实用性不高）。*

3. 可以匹配非闭合元素，如 input 元素、img 元素和 hr 元素等

#### 10.2.1 对:empty伪类可能的误解

1. :empty伪类与空格

如果元素内有注释、有换行、有空格，都无法匹配:empty伪类。
**没有闭合标签的闭合元素也无法匹配:empty伪类**，浏览器会自动补全 html 标签。例如，段落元素可以直接写成：

```html
<p class="cs-empty">
<p class="cs-other">
  <!-- 浏览器自动补全的内容将一直延伸到下一个标签元素的开头，产生换行，不会g匹配 -->
<p class="cs-empty">
</p>
<p class="cs-other">
</p>
<!-- 首尾相连即可匹配 -->
<p class="cs-empty">
<p class="cs-other">
```

2. ::before和::after为元素不会影响 :empty 伪类的匹配

#### 10.2.2 实用场景

1. 隐藏空元素

2. 字段缺失智能提示（::before或::after伪元素给content）

3. 请求加载或空结果提示（原理同上）

可以实用通用类名使整站提示信息保持统一。

### 10.3 子索引伪类

如果想要匹配文字，只有::first-line和::first-letter两个伪元素可以实现，且只有部分CSS属性可以应用。

#### 10.3.1 :first-child伪类和:last-child伪类

:first-child伪类可以匹配第一个子元素，:last-child伪类可以匹配最后一个子元素。

:last-child 从CSS3时代开始出现，兼容性不如前者。

若想列表上下都有 20px 的间距，则下面两种实现都是可以的：

```css
/* 第一种 */
li {
  margin-top: 20px;
}

li:first-child {
  margin-top: 0;
}

/* 第二种 */
li {
  margin-bottom: 20px;
}

li:last-child {
  margin-top: 0;
}

/* 如果不需要兼容IE8浏览器，建议使用:not伪类 */
li:not(:last-child) {
  margin-bottom: 20px;
}
```

#### 10.3.2 :only-child伪类

:only-child可以匹配没有任何兄弟元素的元素，并且会忽略前后的文本内容节点。

某个加载模块里面可能就只有一张加载图片，也可能仅仅就是一段加载描述文字，也可能是加载图片和加载文字同时出现，此时可以使用 :only-child 实现原本需要 JavaScript 逻辑判断的样式渲染。

#### 10.3.3 :nth-child()伪类和:nth-last-child()伪类

:nth-last-child()伪类是从后面开始按指定序号匹配，而:nth-child()伪类是从前面开始匹配。除此之外者没有其他区别，无论是在兼容性还是语法方面。

:nth-chil()伪类虽然功能很强大，但只适用于内动态、无法确定的匹配场景。如果数据是纯静态的，哪怕是列表，都请使用类名或者属性选择器进行匹配，因为此时使用:nth-child()伪类会增加选择器的优先级，且DOM结构严格匹配，无法随意调整，不利于维护。

:nth-child()伪类可以匹配指定索引序号的元素，支持一个参数，且参数必须有，参数可以是：

1. 关键字值

**odd**：匹配第奇数个元素
**even**：匹配第偶数个元素

2. 函数符号

**An+B**：其中A和B都是固定的数值，且必须是整数；n可以理解为从1开始的自然序列（0, 1, 2, 3, …），n前面可以有负号。第一个子元素的匹配序号是1，小于1的计算序号都会被忽略。

***示例：***

* **tr:nth-child-(odd)**：匹配表格的第1, 3, 5行，等同于 tr:nth-child(2n+1)。
* **tr:nth-child(even)**：匹配表格的第2, 4, 6行，等同于 tr:nth-child(2n)。
* **nth-child(3)**：匹配第3个元素。
* **nth-child(5n)**：匹配第5, 10, 15, …个元素，其中5=5×1, 10=5×2, 15=5×3……
* **nth-child(3n+4)**：匹配第4, 7, 10, …个元素，其中4=(3×0)+4, 7=(3×1)+4, 10=(3×2)+4……
* **:nth-child(-n+3)**：四配前3个元素。因为-0+3=3, -1+3=2, -2+3=1。
* **li:nth-child(n)**：匹配所有的 li 元素，就匹配的元素而言和 li 标签选择器一模一样，区别就是优先级更高了（不建议使用）。
* **li:nth-child(1)**：匹配第一个 li 元素和 li:first-child 匹配的作用一样，区别就是后者的兼容性更好。
* **li:nth-child(n+4):nth-child(-n+10)**：匹配第4~10个 li 元素

**动态列表数量匹配技术**

聊天软件中的群头像或者一些书籍的分组往往采用复合头像作为一个大的头像，头像数量不同，布局也会不同。通常会统一编号并分别给样式，但修改成本比较高。

我们可以借助伪类判断当前列表的个数，示意如下：

```css
/* 1个 */
li:only-child {}

/* 2个 */
li:first-child:nth-last-child(2) {}

/* 3个 */
li:first-child:nth-last-child(3) {}

/* …… */
/* 再配合兄弟节点选择符就可以实现不同数量的布局样式 */
```

### 10.4 匹配类型的子索引伪类

匹配类型的子索引伪类类似于子索引伪类，区别在于匹配类型的子索引伪类是在同级中相同标签元素之间进行索引与解析的。

#### 10.4.1 :first-of-type伪类和:last-of-type伪类

:first-of-type表示当前标签类型元素的第一个，:last-of-type匹配最后一个同类型的标签元素。

#### 10.4.2 :only-of-type伪类

:only-of-type表示匹配唯一的标签类型的元素

匹配:only-child的**元素**一定匹配:only-of-type伪类，但匹配:only-of-type伪类的元素不一定匹配:only-child伪类。

#### 10.4.3 :nth-of-type()伪类和:nth-last-of-type()伪类

异同之处类似，具体略。适用于特定标签组合且这些组合不断重复的场合，比如“dt + dd”组合。

## 第11章 逻辑组合伪类

### 11.1 :not()伪类

:not()是否定伪类，如果当前元素与括号里面的选择器不匹配，则该伪类会进行匹配。

1. :not()伪类的优先级是0，即它本身没有任何优先级，最终选择器的优先级是由括号面的达式决定的。
2. :not()伪类可以不断级联。
3. :not()伪类目前尚未支持多个表达式，也不支持出现选择符。

在实现选项卡切换效果的时候会默认藏部分选项卡面板，点击选项按钮后通过添加激活状态类名让隐藏的面板再显示，CSS如下:

```css
.cs-panel {
  display: none;
}

.cs-panel.active {
  display: block:
}

/* 使用:not()伪类实现 */
.cs-panel:not(.active) {
  display: none;
}
```

**有多层容器标签时，不要单独使用:not()伪类**

### 11.2 了解任意匹配伪类:is()

#### 11.2.1 :is()伪类与:matches()伪类及:any()伪类之间的关系

先有:any()伪类，不过其需要配合私有前缀使用，后来因为选择器的优级不准确（忽略括号里面选择器的优先级，而永远是普通伪类的优先级），:any()伪类被舍弃，成为:matches()伪类，然后又因为:matches()伪类的名字太长，最近又修改成了:is()伪类。

#### 11.2.2 :is()伪类的语法与作用

参数可以是复杂选择器或复杂选择器列表，下面的写法都是合法的：

```css
/* 简单选择器 */
:is(article) p {}

/* 简单选择器列表 */
:is(article, section) p {}

/* 复杂选择器 */
:is(.article[class], section) p {}

/* 带逻辑伪类的复杂选择器 */
.some-class:is(article:not([id]), section) p {}
```

有序列表和无序列表可以相互嵌套。假设有两层嵌套关系，则最里面的 li 元素就存在下面4种可能场景:

```css
ol ol li,
ol ul li,
ul ul li,
ul ol li {
  margin-left: 2em;
}

/* 使用:is()伪类进行强化 */
:is(ol, ul) :is(ol, ul) li {
  margin-left: 2em;
}
```

### 11.3 任意匹配伪类:where()

:where()伪类和:is()伪类的含义、语法、作用一模一样，但是:where()伪类的**整体优先级永远是0**。

### 11.4 关联伪类:has()

可以实现类似“父选择器”或“前面兄弟选择器”的功能，其规范早已制定，但DOM渲染机制导致目前浏览器基本都没有支持。

## 第12章 其他伪类选择器

### 12.1 与作用域相关的伪类

#### 12.1.1 参考元素伪类:scope

曾经有一段时间，部分浏览器曾经支持过“在一个网页文档中支持多个CSS作用域”，语法是在 style 元素上设置 scoped 属性。在一番争论之后，这个特性被舍弃了，原本支持它的浏览器也不支持了，scoped属性也被移除了。
然而，:scope伪类却被保留了下来，而且除了IE，其他浏览器都支持。但是如今的网页只有一个CSS作用域，所以:scope伪类等同于:root伪类。可以用来安全地区分IE/非IE浏览器。

但是它在一些 DOM API 中却表现了真正的语义，这些API包括 querySelector()、querySelectorAll()、matches和 Element.closest()。此时:scope伪类匹配的是正在调用这些API的DOM元素。

```javascript
document.queryselector('#myId').querySelectorAll(':scope div div');
```

此时“:scope div div”中的 :scope 匹配的就是 #myId 元素，不添加 :scope 时永远都在全局查找。

#### 12.1.2 Shadow树根元素伪类:host

用于创建Shadow DOM使样式不受外部影响。

#### 12.1.3 Shadow树根元素匹配伪类:host()

可以根据根元素的ID、类名或者属性进行区别匹配。

#### 12.1.4 Shadow树根元素上下文匹配伪类:host-context()

可以借助Shadow DOM根元素的父元素进行匹配，兼容性有限。

### 12.2 与全屏相关的伪类:fullscreen

桌面浏览器以及部分移动端浏览器是支持原生全屏效果的，通过dom.requestFullScreen()方法可让元素全屏显示，通过 document.cancelFullScreen()方法可取消全屏。
:fullscreen伪类是用来匹配处于全屏状态的dom元素的，:backdrop伪元素是用来匹配浏览器默认的黑色全屏背景元素的。

### 12.3 语言相关伪类

#### 12.3.1 方向伪类:dir()

在实际开发时，我们有时候希望布局的元素是从右往左排列的。例如，实现微信或者QQ的左右对话效果，右侧的对话布局就可以直接添加 HTML dir 属性控制实现。

用传统的实现方法，我们会使用属性选择器进行匹配。例如：

```css
[dir="rtl"] .cs-avatar {}
```

但是，这个属性选择器有一个比较明显的缺点，即它无法直接匹配没有设置dir属性的元素，也无法准确知道没有设置dir属性元素的准确的方向，因为dir带来的文档流方向是具有继承性的。**目前只有Firefox支持。**

#### 12.3.2 语言伪类:lang

:lang()伪类用来匹配指定语言环境下的元素。

一个标准的 XHTML 文档结构会在 html 元素上通过HTML lang属性标记语言。对于简体中文站点，建议使用zh-cmn-Hans：

```html
<!DOCTYPE html>
<html lang="zh-cmn-Hans">

<head>
  <meta charset="UTF-8">
</head>

<body>
</body>

</html>
```

对于英文站点或者海外服务器，常使用en：

```html
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
</head>

<body>
</body>

</html>
```

:lang()伪类的典型示例是 CSS quotes 属性的引号匹配。例如：

```css
:lang(zh)>q {
  quotes: '“''”';
}

:lang(en)>q {
  quotes: '\\201C''\\201D''\\2018''\\2019';
}
```

```html
<p lang="zh"><q>中文</q></p>
<p lang="en"><q>英语<q>引号内嵌套的引号</q></q></p>
```

如果着眼于实际开发，我们是不会遇到上面这个使用引号的场景的，更常见的反而用:lang()伪类来实现资源控制。可以根据:lang()的不同使用不同的资源或者呈现不一样的布局。例如，国内的主要社交平台是微信、微博，国外的主要社交平台是脸书、推特，此时，可以借助:lang()伪类呈现不同的分享内容。

:lang()伪类相对于lang属性选择器有以下优点：

1. 即使当前元素没有设置HTML lang属性，也能够准确匹配。
2. 伪类参数中使用的语言代码无须和HTML lang属性值一样，只要包含即可。
3. 兼容性非常好。

### 12.4 资源状态伪类

**Video/Audio播放状态伪类:playing和:paused**

目前无浏览器支持。
